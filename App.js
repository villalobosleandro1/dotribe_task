/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {YellowBox, AppState, Alert, Platform, StatusBar} from 'react-native';
import Meteor from "react-native-meteor";
// import { Buffer } from 'buffer';


import NavigationService from './src/NavigationService';
import RouteNavigator from './src/RouteNavigator';
import store from './src/store';
import {clearUser, logout} from './src/actions';

// import {pushNotifications} from './src/services';
// pushNotifications.configure();

// import Amplify, {Storage} from 'aws-amplify';
import aws_exports from './src/aws-exports';
import {View} from "react-native-reanimated";
// Amplify.configure(aws_exports);

global.Constant = require('./src/lib/constant');
global.Util = require('./src/lib/util');
global.Api = require('./src/lib/api');
global.ApiRoutes = require('./src/lib/apiRoutes');
global.Subscribe = require('./src/subscribes');
global.moment = require('moment');
// global.Buffer = Buffer;
// global.AWS_Storage = Storage;

YellowBox.ignoreWarnings(['Require cycle:']);
console.disableYellowBox = true;

// const SERVER_URL = `${Constant.API.wsProtocol}://${Constant.API.url}/websocket`;
// Meteor.connect(SERVER_URL);

class App extends Component {

  state = {};

  componentDidMount() {

    if(Platform.OS === 'android') {
      StatusBar.setBackgroundColor(Constant.colors.darkBlue);
    }

    // let intentReconect = 0;

    // Meteor.ddp.on('connected', function() {
    //   intentReconect = 0;
    //
    //   const auth = store.getState().authentication;
    //   if(!auth.isLogged) {
    //     // NavigationService.navigate('Auth');
    //   }
    // });

    // Meteor.ddp.on('disconnected', async function() {
    //
    //   const auth = store.getState().authentication;
    //   if(!auth.isLogged) {
    //     NavigationService.navigate('NoConection');
    //   }
    // else {
    //     intentReconect+=1;
    //
    //     if (intentReconect > 2) {
    //       try {
    //         await AsyncStorage.removeItem('token');
    //         store.dispatch(clearUser());
    //         store.dispatch(logout());
    //         // NavigationService.navigate('Auth');
    //       } catch (e) {}
    //
    //     }else{
    //       Alert.alert('No connection to server', 'wait a moment... (' + intentReconect +'/2)');
    //     }
    //   }
    // });

  }
  componentWillUnmount() {
    let meteorStatus = Meteor.status().connected;
    if (meteorStatus) {
      Meteor.disconnect();
    }
  }

  render() {

    return (
        <Provider store={store}>
          <RouteNavigator/>
        </Provider>
    );
  }
}

export default App;
