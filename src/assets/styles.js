import {StyleSheet} from 'react-native';

const {colors} = require('./../lib/constant');

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    marginHorizontal: 15
  },

  simpleContainer: {
    flex: 1,
    alignSelf: 'stretch',
    // backgroundColor: '#f5f5f5',
  },

  bgSecondary: {backgroundColor: colors.secondaryLightColor},
  bgSecondaryGray: {backgroundColor: colors.secondaryGrayColor},
  bgPrimaryDark: {backgroundColor: colors.primaryDarkColor},

  f10: {fontSize: 10},
  f11: {fontSize: 11},
  f12: {fontSize: 12},
  f14: {fontSize: 14},
  f16: {fontSize: 16},
  f18: {fontSize: 18},
  f20: {fontSize: 20},
  f22: {fontSize: 22},
  f24: {fontSize: 24},
  f26: {fontSize: 26},

  fwb: {fontWeight: 'bold'},
  fwn: {fontWeight: 'normal'},
  textWhite: {color: '#FFFFFF'},
  mv10: {marginVertical: 10},
  mv5: {marginVertical: 5},
  pv5: {paddingVertical: 5},
  pv10: {paddingVertical: 10},


  p5: {padding: 5},
  p10: {padding: 10},
  p15: {padding: 15},
  p20: {padding: 20},
  p25: {padding: 25},

  br4: {borderRadius: 4},
  br5: {borderRadius: 5},
  br15: {borderRadius: 15},
  br50: {borderRadius: 50},
  bw1: {borderWidth: 1},

  justifyStart: {justifyContent: 'flex-start'},
  justifyCenter: {justifyContent: 'center'},
  justifySpaceBetween: {justifyContent: 'space-between'},
  justifySpaceAround: {justifyContent: 'space-around'},
  justifyEnd: {justifyContent: 'flex-end'},

  alignItemsCenter: {alignItems: 'center'},
  alignItemsStart: {alignItems: 'flex-start'},
  alignItemsEnd: {alignItems: 'flex-end'},
  alignItemsStretch: {alignItems: 'stretch'},

  alignSelfCenter: {alignSelf: 'center'},

  textMuted: {color: '#888888'},
  margin_h_25: {marginHorizontal: 25},
  margin_h_24: {marginHorizontal: 24},
  margin_h_15: {marginHorizontal: 15},
  margin_h_10: {marginHorizontal: 10},

  padding_h_15: {paddingHorizontal: 15},

  containerTitleTasks: {
    flex: 1,
    justifyContent: 'center'
  },

  containerInfoTasks: {
    flex:  1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  btnNewTask: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center'
  },

  btn: {
    backgroundColor: 'blue',
    color: 'white'
  },

  navBarRightIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: 55,
    height: 55
  },

  row: {
    flex: 1,
    flexDirection: 'row'
  },

  column: {
    flex: 1,
    flexDirection: 'column'
  },

  column1: {flex: 1},
  column2: {flex: 2},
  column3: {flex: 3},
  column4: {flex: 4},
  column5: {flex: 5},
  column6: {flex: 6},
  column7: {flex: 7},
  column8: {flex: 8},
  column9: {flex: 9},

  divider: {
    backgroundColor: colors.secondaryLightColor,
    height: 1
  },

  stretch: {alignSelf: 'stretch'},
  center: {justifyContent: 'center', alignItems: 'center'},

  tasksContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'stretch'
  },

  textInputStyle: {
    borderBottomWidth: 1,
    borderBottomColor: colors.secondaryGrayColor,
    marginBottom: 5
  },

  containerTypeAhead: {
    borderWidth: 1
  },

  circle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },


  // CONVERSATION VIEWS
  conversationItemLabel: {
    fontSize: 12,
    color: '#888888'
  },

  conversationItemContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
    marginVertical: 20
  },

  conversationItemContentLabels: {
    flex: 7,
    alignSelf: 'stretch',
    paddingLeft: 15
  },

  modalUploadFileOrImage: {
    justifyContent: 'flex-end',
    alignItems: 'center'
  },

  containerBtnModalBottom: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: '20%',
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 5
  },

  modalBottomContainerDashboard: {
    height: '50%',
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 4,
  },

  btnModalUploadFile: {
    backgroundColor: colors.secondaryLightColor,
    borderRadius: 30,
    height: 60,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },

  btnModalUploadImage: {
    backgroundColor: colors.primaryDarkColor,
    borderRadius: 30,
    height: 60,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },

  containerBtnLogin: {
    marginTop: 20,
    height: 40
  },

  imageBtnLogin: {
    height: 40,
    width: 40
  },

  logoLogin: {
    flex: .7,
    alignItems:'center',
    justifyContent: 'flex-end',
    alignSelf: 'stretch',
    // backgroundColor: 'red'
  },

  loginContainerButtons: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems:'center',
    justifyContent: 'flex-start',
    marginTop: 30
  },

  previewFileNavBarBottom: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    alignSelf: 'stretch',
    bottom: 10,
    marginHorizontal: 25
  },

  previewFileNavBarBottomBtn: {
    borderRadius: 30,
    height: 60,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },

  previewFileContent: {
    flex: 1,
    alignSelf: 'stretch'
  },

  previewFileDocument: {
    flex: 6,
    alignItems: 'center',
    justifyContent: 'center'
  },

  previewFileDocumentText: {
    fontWeight: 'bold',
    fontSize: 18,
    marginHorizontal: 25
  },

  previewFileBottomBtn: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 15
  },

  viewCamera: {
    flex: 1,
    justifyContent: 'flex-end',
    alignSelf: 'center'
  },

  btnCaptureImage: {
    borderRadius: 30,
    height: 60,
    width: 60,
    padding: 5,
    margin: 10,
    justifyContent: 'center',
    backgroundColor: colors.secondaryLightColor
  },

  btnTopShadow: {
    borderTopWidth: 1,
    borderColor: '#dddddd',
    // borderRadius:1,
    // borderBottomWidth: 0,
    // shadowColor: '#000000',
    // shadowOffset: {width: 0, height: 0},
    // shadowOpacity: 0.5,
    // elevation: 2
  },

  modalDashboardOptionItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 9,
    paddingVertical: 8,
    // borderBottomWidth: 1,
    // borderBottomColor: colors.darkBlue
  },

  remitentChatList: {
    alignSelf: 'stretch',
    width: '100%',
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },

  sugestionContact: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    flex: 1,
    marginVertical: 5,
    justifyContent: 'center'
  },

  viewAvatarContact: {
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center'
  },

  textLabelContact: {
    width: '80%',
    justifyContent: 'center'
  },

  flexRow: {
    flexDirection: 'row'
  },

  height50: {
    height: 50
  },

  alignTextCenter: {
    textAlign: 'center'
  },

  btnNewConversation: {
    bottom: 15,
    alignItems: 'flex-end',
    alignSelf: 'stretch',
    marginRight: 20,
  },

  containerLeft: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginVertical: 5,
    height: 250,
    width: 200,
    borderRadius: 5
  },

  containerImage: {
    width: '100%',
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerText: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    marginRight: 10,
  },
  containerDocument: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    height: 120,
    width: 100,
    borderRadius: 5
  },

  suggestionsLead: {
    borderWidth: 2,
    borderRadius: 5,
    borderColor: colors.secondaryGrayColor,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerNoConection: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    marginHorizontal: 15
  },
  containerImageNoConection: {
    height: 220,
    width: 220,
    alignItems: 'center',
    marginTop: 100
  },
  imageNoConection: {
    height: '100%',
    width: '100%',
    borderRadius: 180
  },

  textContent: {
    top: 80,
    height: 50,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    width: '100%',
    color: 'black'
  },

  marginT10: {marginTop: 10},
  marginT15: {marginTop: 15},
  marginT20: {marginTop: 20},

  width200: {width: 200},
  width150: {width: 150},

  fontRobotoRegular: {fontFamily: 'Roboto-Regular'},
  fontRobotoBold: {fontFamily: 'Roboto-Bold'},
  fontRobotoLight: {fontFamily: 'Roboto-Light'},

  height100: {height: 100},

  buttonCreateList: {
    borderColor: colors.darkBlue,
    height: 40,
    paddingHorizontal: 10
  },
  buttonSaveList: {
    width: 120,
    height: 30,
    borderColor: colors.darkBlue,
    paddingHorizontal: 10
  },

  swipeableRight: {
    marginLeft: -200,
    backgroundColor: colors.redError,
    width: '35%',
    marginBottom: 5
  },

  swipeableLeft: {
    marginRight: -200,
    backgroundColor: colors.greenSuccess,
    width: '35%',
    marginBottom: 5
  }


});