// import Meteor from "react-native-meteor";
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';

class Api {

  login(credentials) {
    const data = {
      usernameOrEmail: credentials.username,
      password: credentials.password
    };

    return new Promise(async (resolve, reject) => {
      try {
        Axios({
          method: 'post',
          url: `${Constant.API.oauthUrl}${ApiRoutes.LOGIN}`,
          headers: {
              'x-secret': '12345abc',
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*'
          },
          data: {data}
        })
            .then(function (response) {
              console.log('rnlog response', response);
              AsyncStorage.setItem('token', response.data.token);
              resolve(response.data.token);
            })
            .catch(function (error) {
              console.error('rnlog error ', error);
              reject(error);
            });

        // Meteor.call(ApiRoutes.LOGIN, {
        //   user: {
        //     email: credentials.username,
        //   },
        //   password: credentials.password
        // }, (error, response) => {
        //   if (error) {
        //     reject(error);
        //   }
        //   if (response) {
        //     AsyncStorage.setItem('token', response.token);
        //     resolve(response.token);
        //   }
        //
        // });
      } catch(e) {
        Alert.alert('E', e);
      }

    });
  }

  logout() {
    return new Promise(async (resolve, reject) => {

      try {
        const token = await AsyncStorage.getItem('token');
        // console.log('rnlog token ', token);

        Axios({
          method: 'post',
          url: `${Constant.API.oauthUrl}${ApiRoutes.LOGOUT}`,
          headers: {
            'x-secret': '12345abc',
            'x-token': token
          }
        })
            .then(function (response) {
                console.log('rnlog response ', response);
                AsyncStorage.removeItem('token');
                resolve(true);

            })
            .catch(function (error) {
                console.log('rnlog error ', error);
              reject(error);

            });

        // Meteor.call(ApiRoutes.LOGOUT, token, async (error, response) => {
        //   if (error) {
        //     reject(error);
        //   } else {
        //     await AsyncStorage.removeItem('token');
        //     resolve(true);
        //   }
        //
        // });

      } catch (error) {
        console.log('logout error', error);
      }

    });
  }

  get (name, params = null) {
      return this.call(name, params, 'GET');
  }

  post (name, data = null) {
      return this.call(name, data, 'POST');
  }

  call(name, data = null, method = 'GET') {
      // console.log('rnlog api ', name, data, method);
    return new Promise(async (resolve, reject) => {
        try {
          const token = await AsyncStorage.getItem('token');
          const url = `${Constant.API.url}${name}`;
          const config = {
              method: method.toUpperCase(),
              timeout: 5000,
              url: url,
              headers: {
                  'Content-Type': 'application/json',
                  'x-secret': '12345abc',
                  'x-token': token
              }
          };

            if(config.method === 'GET') {
                if(data && Object.keys(data).length > 0) {
                    config.params = data;
                }
            }

            if(config.method === 'POST' || config.method === 'PUT') {
                if(data && Object.keys(data).length > 0) {
                    config.data = data;
                    // console.log(`rnlog DATA ${JSON.stringify(config.data)}`);
                }
            }

          // const access_token = {extraData: token};
          //
          // data = Object.assign(data, access_token);
            console.log('rnlog config', config);
          Axios(config)
              .then(function ({data}) {
                console.log('rnlog data', data);
                  if (typeof data === 'object') {
                    if (typeof data.success !== 'undefined' && data.success) {
                      resolve(data.data);
                    } else if (typeof data.success !== 'undefined' && !data.success) {
                      reject(data);
                    } else {
                      resolve(data);
                    }
                  } else {
                    resolve(data);
                  }

                // console.log('rnlog response ', response);
              })
              .catch(function (error) {
                Alert.alert('Error', error);
              });

          // Meteor.call(name, data, (error, response) => {
          //   if (error) reject(error);
          //
          //   if (typeof response === 'object') {
          //     if (typeof response.success !== 'undefined' && response.success) {
          //       resolve(response);
          //     } else if (typeof response.success !== 'undefined' && !response.success) {
          //       reject(response);
          //     } else {
          //       resolve(response);
          //     }
          //   } else {
          //     resolve(response);
          //   }
          // });

        } catch (error) {
          console.log('call error', error);
        }
      // }

    });
  }

}

module.exports = new Api();
