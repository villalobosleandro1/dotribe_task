import aws_exports from './../aws-exports';

module.exports = {
  titleApp: 'DoTribe Task',
  API: {
    // url: 'https://46c987c5.ngrok.io',
    // oauthUrl: 'https://bfb8dc98.ngrok.io',
    url: 'http://192.168.30.55:3005/api/v1',
    oauthUrl: 'http://192.168.30.55:22344',
    // port: '5001',
  },
  colors: {
    primaryLightColor: '#ffffff',
    secondaryLightColor: '#2490E8',
    primaryDarkColor: '#FFB001',
    primaryContainersColor: '#29205a',

    primaryDangerColor: '#f35a57',
    primaryGrayColor: '#808285',
    secondaryGrayColor: '#9d9fa2',
    grayColor: '#cccccc',

    darkBlue: '#00215a',
    redError: '#dc3545',
    greenSuccess: '#28a745'
  },
  image: {
    btnLogin: require('./../assets/images/logoBtn.png'),
    logo: require('./../assets/images/logo.png'),
    backgroundLogin: require('./../assets/images/background1.jpg'),
    noConection: require('./../assets/images/noConection.jpg'),
    dashboardEmpty: require('./../assets/images/dashboard-empty.jpg'),
    descriptionEmpty: require('./../assets/images/description-empty.jpg'),
    noConversation: require('./../assets/images/noConversation.png'),
    backgroundChat: require('./../assets/images/backgroundChat.jpg'),
    noMessages: require('./../assets/images/noMessages.png')
  },
  AWS: {
    baseUrl: `https://s3.amazonaws.com/${aws_exports.aws_user_files_s3_bucket}/public/`,
    bucket: aws_exports.aws_user_files_s3_bucket
  },
  splash : require('./../assets/videos/task_white.mp4'),
  iconProps: {
    width: 30,
    height: 45,
    strokeWidth: 20
  }
};
