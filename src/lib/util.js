module.exports = {
  getToday: (date) => {
    let today = new Date(date);
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let hour = today.getHours();
    let minutes = today.getMinutes();
    let seconds = today.getSeconds();

    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if (seconds < 10) {
      seconds = '0' + seconds;
    }
    today = yyyy + '-' + mm + '-' + dd + ' ' + hour + ':' + minutes + ':' + seconds;
    return today;
  },

  generateNotificationId: (min = 100000, max = 999999) => {
    // Math.floor(Math.random() * (MAX - MIN)) + MIN
    let pin = Math.floor(Math.random() * (max - min)) + min;
    return pin;
  }
};

