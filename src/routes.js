import Login from "./containers/Login";
import Home from "./containers/Home";
import Conversations from "./containers/Conversations";
import Messages from "./containers/Messages";
import PreviewFile from "./components/PreviewFile";
import TakeAPicture from "./containers/TakeAPicture";
import SearchContact from "./containers/SearchContact";
import TaskDetails from './containers/TaskDetails';

const routes = {
  dashboard: {screen: Home, navigationOptions: ({navigation}) => ({title: 'DASHBOARD', headerLeft: null})},
  conversations: {screen: Conversations, navigationOptions: ({navigation}) => ({title: 'CONVERSATION LIST'})},
  messages: {screen: Messages, navigationOptions: {header: null}},
  previewFile: {screen: PreviewFile, navigationOptions: ({navigation}) => ({title: 'PREVIEW'})},
  takeAPicture: {screen: TakeAPicture, navigationOptions: ({navigation}) => ({title: 'TAKE' +
      ' PICTURE'})},
  searchContact: {screen: SearchContact, navigationOptions: ({navigation}) => ({title: 'SEARCH'})},
  taskDetails: {screen: TaskDetails, navigationOptions: ({navigation}) => ({title: 'TASK DETAILS'})},
};

const authRoutes = {
  login: {screen: Login, navigationOptions: ({navigation}) => ({header: null})}
};

export default {authRoutes, routes};