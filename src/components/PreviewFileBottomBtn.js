import React from 'react';
import {View, TouchableOpacity, Alert} from 'react-native';
import {Icon} from 'react-native-elements'
import {connect} from 'react-redux';
import {DocumentPicker, DocumentPickerUtil} from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import Spinner from "react-native-loading-spinner-overlay";

import NavigationService from './../NavigationService';
import styles from './../assets/styles';
import {takePicture, clearFile, setFile, selectConversation} from "./../actions";
import Toast from "react-native-simple-toast";


class PreviewFileBottomBtn extends React.Component {

  state = {
    false: true,
    disableBtn: false
  };

  _upload = () => {
    this.setState({loading: true, disableBtn: true});

    const conversation = this.props.conversation;

    this.readFile(this.props.file.uri).then(buffer => {
      let ext = this.props.file.type.split('/')[1],
        random = Util.generateNotificationId(1000000000, 9999999999),
        fileName = `${random}.${ext}`;

      AWS_Storage.put(fileName, buffer, {contentType: this.props.file.type})
        .then (result => {
          let key = result.key,
            insert = {
              mediaUrl: Constant.AWS.baseUrl + key,
              fileType: this.props.file.type,
              to: conversation.who,
              leadId: conversation.leadId,
              contactId: conversation.contactId
            };

        Api.call(ApiRoutes.SEND_SMS, insert)
          .then(response => {
            this.props.selectConversation({_id: response.data._id, who: response.data.who, labelContact: response.data.labelContact});
            this._reset();
          })
          .catch(error => {
            // Alert.alert('Error', 'message not sent, try' +
            // ' later.')
            Toast.show(`ERROR ${error.message}`, Toast.LONG);
          });

        }).catch(err => {
          // console.log('rnlog RESULT ERROR', err)
          Toast.show(`ERROR ${err.message}`, Toast.LONG);
      });

    }).catch(e => {
      // console.log('rnlog Error readfile', e)
      Toast.show(`ERROR ${e.message}`, Toast.LONG);
    });

  };

  readFile(filePath) {
    return RNFetchBlob.fs.readFile(filePath, 'base64').then(data => new Buffer(data, 'base64'));
  }

  _reset = () => {
    this.setState({loading: false, disableBtn: false});
    this.props.takePicture();
    this.props.clearFile();
    NavigationService.navigate('messages');
  };

  _takeAnother = () => {
    if(this.props.tp.pictureTaken) {
      this.props.takePicture();
      NavigationService.navigate('takeAPicture');
    } else {
      this._takeAnotherFile();
    }
  };

  _takeAnotherFile = () => {
    let type = {
      filetype: [],
    };

    if(this.props.file.typeFile) {
      switch (this.props.file.typeFile) {
        case 'file':
          type.filetype.push(DocumentPickerUtil.pdf());
          break;
        case 'image':
          type.filetype.push(DocumentPickerUtil.images());
          break;
      }
    }

    DocumentPicker.show(type,(error, res) => {
      if(error) {
        NavigationService.navigate('messages');
      }else{
        res.typeFile = this.props.file.typeFile;
        this.props.setFile(res);
        NavigationService.navigate('previewFile');
      }
    });
  };

  render(){

    let icon = 'file-restore';
    if(this.props.file.typeFile === 'image') {
      icon = 'camera-party-mode';
    }

    const btn = [
      {
        icon,
        backgroundColor: Constant.colors.primaryContainersColor,
        fun: this._takeAnother
      },
      {
        icon: 'close',
        backgroundColor: Constant.colors.primaryDangerColor,
        fun: this._reset
      },
      {
        icon: 'arrow-expand-up',
        backgroundColor: Constant.colors.primaryDarkColor,
        fun: this._upload
      }
    ];

    return(
      <View style={styles.previewFileNavBarBottom}>
        {
          this.state.loading &&
          <Spinner
            animation={'fade'}
            visible={this.state.loading}
            textStyle={{textAlign: 'center', width: '100%', color: Constant.colors.darkBlue}}
            textContent={"Loading..."}
            size={"large"}
            overlayColor={'rgba(255, 255, 255, 0.40)'}
            color={Constant.colors.darkBlue}
          />
        }

        <View style={styles.previewFileNavBarBottom}>

          {
            btn.map((b, i) => (
              <TouchableOpacity
                key={i}
                onPress={b.fun}
                style={[styles.previewFileNavBarBottomBtn, {backgroundColor: b.backgroundColor}]}
                disabled={this.state.disableBtn}
              >
                <Icon
                  name={b.icon}
                  size={30}
                  color={Constant.colors.primaryLightColor}
                  type='material-community'
                />
              </TouchableOpacity>
            ))
          }

        </View>
      </View>
    )
  }
}

const mapStateToProps = ({modal, file, takePicture, conversations}) => ({
  modal,
  file,
  tp: takePicture,
  conversation: conversations.conversation
});

const mapDispatchToProps = (dispatch) => ({
  takePicture: () => dispatch(takePicture()),
  clearFile: () => dispatch(clearFile()),
  setFile: (payload) => dispatch(setFile(payload)),
  selectConversation: (conversation) => dispatch(selectConversation(conversation))
});

export default connect(mapStateToProps, mapDispatchToProps)(PreviewFileBottomBtn);