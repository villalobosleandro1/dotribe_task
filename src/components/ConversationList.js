import React from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
// import Meteor from "react-native-meteor";
import Spinner from 'react-native-loading-spinner-overlay';
import SocketIOClient from 'socket.io-client';

import styles from './../assets/styles';
import ConversationItem from './ConversationItem';
import NavigationService from "../NavigationService";
import MessageIcon from './icons/MessageIcon';

class ConversationList extends React.Component {

  state = {
    socket: SocketIOClient('http://192.168.88.55:9999'),
    conversations: [],
    loading: true,
    colorCombination:[
      {
        color: Constant.colors.primaryLightColor,
        backgroundColor: Constant.colors.primaryDarkColor,
      },
      {
        color: Constant.colors.primaryLightColor,
        backgroundColor: Constant.colors.secondaryLightColor,
      },
    ],
    colorCombinationIdx: 0
  };

  componentDidMount() {
    this.state.socket.connect();
    this.state.socket.emit(Subscribe.CONVERSATIONS, {idPhonePbx: this.props.user.idPhonePbx, limit: 99});
  }

  // componentWillUnmount() {
  //   if(this.props.ready) {
  //     Meteor.subscribe(Subscribe.CONVERSATIONS, {extraData: this.props.auth.token}).stop();
  //   }
  // }

  componentDidUpdate() {
    if(this.props.ready && this.state.loading) {
      this.setState({loading: false})
    }
  }

  _colorCombination = (index) => {
    return (index % 2 === 0) ? {
      color: this.state.colorCombination[0].color,
      backgroundColor:this.state.colorCombination[0].backgroundColor
    } : {
      color: this.state.colorCombination[1].color,
      backgroundColor:this.state.colorCombination[1].backgroundColor
    }
  };

  render() {

    this.state.socket.on(Subscribe.CONVERSATIONS, data => {
      // console.log('rnlog data ', data);
      this.setState({conversations: data, loading: false});
      if(data.length === 0) {
        this.setState({loading: false});
      }
    });

    // const conversations = this.props.conversations;
    // console.log('rnlog conversations ', this.state.conversations);

    return (
      <View style={styles.container}>

        {
          this.state.loading &&
          <Spinner
            animation={'fade'}
            visible={this.state.loading}
            textStyle={{textAlign: 'center', width: '100%', color: Constant.colors.darkBlue}}
            textContent={"Loading..."}
            size={"large"}
            overlayColor={"white"}
            color={Constant.colors.darkBlue}
          />
        }

        {
          this.state.conversations.length === 0 &&
          <View style={[styles.simpleContainer, styles.center]}>
              <Image source={Constant.image.noConversation} style={{width: 188, height: 180}}/>
            <Text style={[styles.f22, styles.textMuted]}>No Conversations</Text>
          </View>
        }

        {
          this.state.conversations.length  >  0 &&
          <ScrollView style={styles.simpleContainer}>
            {
              this.state.conversations.map((conversation, index)=>{
                return (
                  <ConversationItem
                    key={index}
                    conversation={conversation}
                    colors={() => this._colorCombination(index)}
                  />
                )
              })
            }
          </ScrollView>
        }
          <View style={styles.btnNewConversation}>
            <TouchableOpacity onPress={() => NavigationService.navigate('searchContact')}>
              <MessageIcon height={60} width={45}/>
            </TouchableOpacity>
          </View>

      </View>
    )
  }
}

export default ConversationList;
