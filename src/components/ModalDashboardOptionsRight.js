import React from 'react';
import Modal from "react-native-modal";
import styles from "./../assets/styles";
import {Alert, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {connect} from 'react-redux';

import {showOrHideModalDashboardRight, setTaskUndone, setTaskDone, setTasksList, setTasksListSelected, load, logout, clearUser} from "./../actions";
import ModalDashboardOptionsHeader from "./ModalDashboardOptionsHeader";
import HomeIcon from './icons/HomeIcon';
import ChatIcon from './icons/ChatIcon';
import LogoutIcon from './icons/LogoutIcon';
import NavigationService from "../NavigationService";
import Toast from "react-native-simple-toast";

class ModalDashboardOptionsRight extends React.Component {

  _reload = () => {
    Api.call(ApiRoutes.TASKS_LIST_BY_USER_LOGGED_APP)
      .then(response => {
        const result = response.data;
        this.props.setTasksList(result.list);
        this.props.setTasksListSelected(result.listSelected);
        this.props.load();
      })
      .catch(error => {
        if (typeof error.stopAlerts === 'undefined' || !error.stopAlerts) {
          // Alert.alert('Error', error.message);
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        }
      });
  };

  _setTaskDone = () => {
    this.props.setTaskDone();
    this.props.showOrHideModalDashboardRight();
  };

  _setTaskUndone = () => {
    this.props.setTaskUndone();
    this.props.showOrHideModalDashboardRight();
  };

  _completeAll = () => {
    this.props.load();
    Api.call(ApiRoutes.TASK_COMPLETE_ALL, {taskListId: this.props.taskList.selected._id})
      .then(response => {
        this._reload();
        this.props.showOrHideModalDashboardRight();
      })
      .catch(error => Alert.alert('Error', 'the task could not be completed.'));
  };

  _confirmLogout = () => {
    Alert.alert(
      'Are you sure you want to close session?',
      '',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: () => this._logout()},
      ],
      {cancelable: false}
    );
  };

  _logout = () => {
    Api.logout()
      .then(response => {
        this.props.logout();
        this.props.clearUser();
        NavigationService.navigate('Auth');
      })
      .catch(error => {
        // Alert.alert('Error', error.message)
        Toast.show(`ERROR ${error.message}`, Toast.LONG);
      });
  };


  render () {
    return (
      <View>
        <Modal
          isVisible={this.props.showModalDashboardRight}
          swipeDirection={'down'}
          backdropOpacity={.3}
          style={styles.modalUploadFileOrImage}
          onBackButtonPress={() => this.props.showOrHideModalDashboardRight()}
        >
          <View style={styles.modalBottomContainerDashboard}>
            <View style={styles.column1}>

              <ModalDashboardOptionsHeader onPress={this.props.showOrHideModalDashboardRight}/>

              <View style={styles.column9}>
                <ScrollView>
                  {/*<TouchableOpacity style={[styles.modalDashboardOptionItem, {flexDirection: 'row', marginHorizontal: 9}]} onPress={() => NavigationService.navigate('dashboard')}>*/}
                  {/*  <HomeIcon*/}
                  {/*    height={35}*/}
                  {/*  />*/}
                  {/*  <Text style={{marginLeft: 10, color: Constant.colors.darkBlue}}>Home</Text>*/}
                  {/*</TouchableOpacity>*/}

                  <TouchableOpacity style={[styles.modalDashboardOptionItem, {flexDirection: 'row', marginHorizontal: 9}]}
                                    onPress={() => {this.props.showOrHideModalDashboardRight();NavigationService.navigate('conversations');}}>
                    <ChatIcon/>
                    <Text style={{marginLeft: 10, color: Constant.colors.darkBlue}}>Chat</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={[styles.modalDashboardOptionItem, {flexDirection: 'row', marginHorizontal: 9}]} onPress={this._confirmLogout}>
                    <LogoutIcon
                      strokeWidth={25}
                    />
                    <Text style={{marginLeft: 10, color: Constant.colors.darkBlue}}>Log out</Text>
                  </TouchableOpacity>

                  <View style={[styles.alignItemsCenter, styles.justifyEnd]}>
                    {
                      !this.props.taskList.showCompleted &&
                      <TouchableOpacity
                        style={[styles.flexRow, styles.center, styles.bw1, styles.br5, styles.buttonCreateList, styles.width200]}
                        onPress={this._setTaskDone}
                      >
                        <Text style={[styles.fontRobotoLight, {marginLeft: 5, color: Constant.colors.darkBlue}]}>See
                          Task Completed</Text>
                      </TouchableOpacity>
                    }

                    {
                      this.props.taskList.showCompleted &&

                      <TouchableOpacity
                        style={[styles.flexRow, styles.center, styles.bw1, styles.br5, styles.buttonCreateList, styles.width200]}
                        onPress={this._setTaskUndone}
                      >
                        <Text style={[styles.fontRobotoLight, {marginLeft: 5, color: Constant.colors.darkBlue}]}>See Task Uncompleted</Text>
                      </TouchableOpacity>
                    }
                  </View>



                </ScrollView>
              </View>

            </View>

          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = ({taskList, modal}) => ({taskList, showModalDashboardRight: modal.showModalDashboardRight});

const mapDispatchToProps = (dispatch) => ({
  setTasksList: (payload) => dispatch(setTasksList(payload)),
  setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  showOrHideModalDashboardRight: () => dispatch(showOrHideModalDashboardRight()),
  setTaskUndone: () => dispatch(setTaskUndone()),
  setTaskDone: () => dispatch(setTaskDone()),
  load: () => dispatch(load()),
  logout: () => dispatch(logout()),
  clearUser: () => dispatch(clearUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalDashboardOptionsRight);
