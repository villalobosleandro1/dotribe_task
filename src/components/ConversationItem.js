import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

import styles from './../assets/styles';
import {selectConversation} from "./../actions";
import NavigationService from './../NavigationService';

class ConversationItem extends React.Component {

  _selectConversation = () => {
    const {id, who, label, labelContact} = this.props.conversation;
    // console.log('rnlog id ', id);
    this.props.selectConversation({_id: id, who, label, labelContact});
    NavigationService.navigate('messages');
  };

  render() {
    // console.log('rnlog this.props.conversation ', this.props.conversation);
    let lastMessage = '';
    if(this.props.conversation.lastMessage) {
      lastMessage = this.props.conversation.lastMessage;
    }

    return (
      <TouchableOpacity
        style={styles.conversationItemContainer}
        onPress={this._selectConversation}
      >

        <View style={styles.conversationItemContentLabels} >
          <Text style={[styles.f18, styles.fwb, {color: Constant.colors.darkBlue}]}>
            {this.props.conversation.who}
          </Text>

          {
            this.props.conversation.labelContact &&
            <Text style={[styles.f12, {color: Constant.colors.darkBlue, marginTop: 10}]}>
              {this.props.conversation.labelContact}
            </Text>
          }

        </View>

        <View style={[styles.column3, styles.stretch, styles.justifyEnd, styles.alignItemsEnd, {marginTop: 10}]}>
          <Text style={[styles.f12, {color: Constant.colors.darkBlue}]}>
            {lastMessage}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const mapStateToProps = ({conversations}) => ({conversations});

const mapDispatchToProps = (dispatch) => ({
  selectConversation: (conversation) => dispatch(selectConversation(conversation)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConversationItem);
