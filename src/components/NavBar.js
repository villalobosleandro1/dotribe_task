import React from 'react';
import {View, TouchableOpacity, Alert} from 'react-native';
import {Icon} from 'react-native-elements';

import styles from './../assets/styles';

import {connect} from 'react-redux';
import {logout, clearUser} from './../actions';
import NavigationService from './../NavigationService';
import ArrowLeftIcon from './icons/ArrowLeftIcon';

class NavBar extends React.Component {

  _confirmLogout = () => {
    Alert.alert(
      'Are you sure you want to close session?',
      '',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: () => this._logout()},
      ],
      {cancelable: false}
    );
  };

  _logout = () => {
    Api.logout()
      .then(response => {
        this.props.logout();
        this.props.clearUser();
        NavigationService.navigate('Auth');
      })
      .catch(error => Alert.alert('Error', error.message));
  };

  render() {
    return (
      <View style={styles.row}>
        <TouchableOpacity
            onPress={() => NavigationService.navigate('dashboard')}
            style={{marginLeft: 15}}
          >
          <ArrowLeftIcon height={25}/>
        </TouchableOpacity>
      </View>
    )
  }

}

const mapStateToProps = ({authentication}) => ({auth: authentication});
const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
  clearUser: () => dispatch(clearUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);