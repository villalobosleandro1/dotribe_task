import React from 'react';
import {connect} from 'react-redux';
import {TouchableOpacity, View, Text} from 'react-native';
import Modal from "react-native-modal";
import {Icon} from "react-native-elements";

import TaskForm from "./TaskForm";
import styles from "./../assets/styles";
import {showOrHideModalTaskForm} from "./../actions";

class ModalTaskForm extends React.Component {
  render() {
    return (
      <View>
        <Modal
          isVisible={this.props.showModalTaskForm}
          swipeDirection={'down'}
          backdropOpacity={.3}
          style={[
            styles.container,
            styles.stretch,
            styles.alignItemsStretch,
            styles.justifyCenter,
            styles.br4,
            styles.p15
          ]}
          onBackButtonPress={() => this.props.showOrHideModalTaskForm()}
        >

          <View style={[styles.row, styles.center]}>

            <Text style={[styles.column9, styles.f18, styles.alignSelfCenter, {color: Constant.colors.darkBlue}]}>Task Form</Text>

            <TouchableOpacity
              onPress={() => this.props.showOrHideModalTaskForm()}
              style={[styles.column1, styles.center]}
            >
              <Icon
                name={'close'}
                color={Constant.colors.darkBlue}
              />
            </TouchableOpacity>
          </View>

          <View style={[styles.column9]}>
            <TaskForm/>
          </View>

        </Modal>
      </View>
    )
  }
}

const mapStateToProps =  ({modal}) => ({showModalTaskForm: modal.showOrHideModalTaskForm});
const mapDispatchToProps = (dispatch) => ({
  showOrHideModalTaskForm : () => dispatch(showOrHideModalTaskForm())
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalTaskForm);