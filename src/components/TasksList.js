import React from 'react';
import {View, ScrollView, Text, Image, Alert} from 'react-native';
import Swipeable from 'react-native-swipeable';
import {Icon} from 'react-native-elements';
import Toast from "react-native-simple-toast";

import styles from "./../assets/styles";
import TaskItem from './TaskItem';
import DeleteIcon from './icons/DeleteIcon';
import {setTask, setTasksList, setTasksListSelected, showOrHideModalTaskForm} from './../actions';
import {connect} from "react-redux";


class TasksList extends React.Component {

  _reload = () => {
    Api.call(ApiRoutes.TASKS_LIST_BY_USER_LOGGED_APP)
      .then(response => {
        this.props.setTasksList(response.list);
        this.props.setTasksListSelected(response.listSelected);
      })
      .catch(error => {
        if (typeof error.stopAlerts === 'undefined' || !error.stopAlerts) {
          // Alert.alert('Error', error.message);
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        }
      });
  };

  _completeTask = (item) => {
    this.setState({completedTask: true});
    if(!item.completed) {
      Api.post(ApiRoutes.TASK_COMPLETED(item._id))
        .then(response => {
            this._reload();
            this.setState({completedTask: false});
            Toast.show('TASK COMPLETED', Toast.LONG);
        })
        .catch(error => {
          // Alert.alert('Error', error.message)
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        })
    }
  };

  _removeTask = (item) => {
    console.log('rnlog borrar');
    Api.call(ApiRoutes.TASK_REMOVE(item._id), {}, 'DELETE')
      .then(response => {
        this._reload();
        Toast.show('DELETED TASK', Toast.LONG);
      })
      .catch(error => {
        // Alert.alert('Error', error.message)
        Toast.show(`ERROR ${error.message}`, Toast.LONG);
      });
  };

  _confirmRemoveTask = (item) => {
    Alert.alert(
      'Are you sure you want to delete this task?',
      'Once this action is executed, it will not be possible to restore the task.',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: () => this._removeTask(item)},
      ],
      { cancelable: false }
    )
  };

  render() {
    return(
      <View style={[styles.simpleContainer, styles.tasksContainer]}>

        {
          this.props.items.length === 0 &&
          <View style={[styles.simpleContainer, styles.center]}>
            <Image source={Constant.image.dashboardEmpty} style={{width: '80%', height: '80%'}}/>
          </View>
        }

        {
          this.props.items.length > 0 &&
          <ScrollView>
            {
              this.props.items.map((item, index) => {
                const iconName = item.completed ? "checkbox-marked-outline" : "checkbox-blank-outline";
                return (
                  <Swipeable
                    key={index}
                    style={[styles.simpleContainer, styles.center, {backgroundColor:'transparent'}]}
                    rightActionActivationDistance={150}
                    rightContent={(
                      <View ref='MarkerRight' style={[styles.alignSelfCenter, styles.center, styles.height100, styles.swipeableRight]}>
                        <DeleteIcon color={'white'}/>
                        <Text style={{color: Constant.colors.primaryLightColor}}>DELETE TASK</Text>
                      </View>
                    )}

                    leftActionActivationDistance={150}
                    leftContent={(
                      <View ref='MarkerLeft' style={[styles.alignSelfCenter, styles.center, styles.height100, styles.swipeableLeft]}>
                        <Icon
                          name='check'
                          type='material-community'
                          color={Constant.colors.primaryLightColor}
                        />
                        <Text style={{color: Constant.colors.primaryLightColor}}>TASK DONE</Text>

                      </View>
                    )}
                    onRightActionRelease={()=> this._confirmRemoveTask(item)}
                    onLeftActionRelease={()=> this._completeTask(item)}
                  >
                    <TaskItem icon={iconName} item={item} key={index}/>
                  </Swipeable>
                )
              })
            }
          </ScrollView>
        }

      </View>
    );
  }
}


const mapStateToProps = ({modal}) => ({
  showModalTaskForm: modal.showOrHideModalTaskForm
});

const mapDispatchToProps = (dispatch) => ({
  setTask: (payload) => dispatch(setTask(payload)),
  setTasksList: (payload) => dispatch(setTasksList(payload)),
  setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  showOrHideModalTaskForm : () => dispatch(showOrHideModalTaskForm())
});

export default connect(mapStateToProps, mapDispatchToProps) (TasksList);
