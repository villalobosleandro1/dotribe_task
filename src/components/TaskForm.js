import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  FlatList,
  Keyboard,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon} from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';

import {
  clearTask,
  setTasksList,
  setTasksListSelected,
  showOrHideModalTaskForm
} from './../actions';

import styles from './../assets/styles';

import {pushNotifications} from './../services';

class TaskForm extends React.Component {

  state = {
    name: '',
    description: '',
    timeLimit: '',
    lead: '',
    completed: false,
    suggestions: [],
    search: '',
    loading: false
  };

  componentDidMount() {
    const task = this.props.task;
    if (task) {
      // console.log('rnlog otra new Date(task.timeLimit) ', new Date(task.timeLimit));
      // console.log('rnlog otra n) ', task.timeLimit);
      // console.log('rnlog new Date(parseInt()*1000', new Date(parseInt(task.timeLimit)*1000));

      this.setState({
        name: task.name,
        description: task.description,
        timeLimit: task.timeLimit > 0 ? new Date(parseInt(task.timeLimit)*1000) : '',
        completed: task.completed,
        relatedId: task.relatedId,
        relatedLabel: task.relatedLabel,
        relatedCollection: task.relatedCollection,
        appNotificationId: task.appNotificationId
      });
    }
    Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  _keyboardDidShow = () => {
    this.setState({keyboard: true});
  };

  _keyboardDidHide = () => {
    this.setState({keyboard: false});
  };

  componentWillUnmount() {
    this.props.clearTask();
  }

  _reload = () => {
    Api.call(ApiRoutes.TASKS_LIST_BY_USER_LOGGED_APP)
      .then(response => {
        // console.log('rnlog reload ', response);
        this.props.setTasksList(response.list);
        this.props.setTasksListSelected(response.listSelected);
      })
      .catch(error => {
        if (typeof error.stopAlerts === 'undefined' || !error.stopAlerts) {
          // Alert.alert('Error', error.message);
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        }
      });
  };

  _saveTask = () => {
    if (this.state.name === '') {

      this.setState({showErrorName: true});
    } else {
      this.setState({loading: true});

      let appNotificationId = Util.generateNotificationId();

      if (this.state.appNotificationId) {
        appNotificationId = this.state.appNotificationId;
      }

      const task = this.props.task;

      const data = {
        taskListId: this.props.taskList.selected._id,
        name: this.state.name,
        timeLimit: this.state.timeLimit,
        description: this.state.description,
        relatedId: this.state.relatedId,
        relatedCollection: this.state.relatedCollection,
        relatedLabel: this.state.relatedLabel,
        appNotificationId
      };

      if (typeof data.timeLimit === 'undefined' || data.timeLimit === '') {
        data.timeLimit = 0;
      } else {
        data.timeLimit = Date.parse(data.timeLimit);

        if (new Date(data.timeLimit) <= new Date(Date.now())) {
          this.setState({loading: false});
          return Alert.alert('ERROR', 'La fecha y hora no' +
            ' puede ser menor a la actual.');
        }
      }
      // data.timeLimit = Util.getToday(data.timeLimit);

      let url = ApiRoutes.TASK_INSERT;
      let method = 'POST';

      if (task) {
        url = ApiRoutes.TASK_UPDATE(task._id);
        method = 'PUT';
        // data._id = task._id;
      }

      console.log('rnlog ', url, data, method);

      Api.call(url, data, method)
        .then(response => {
          // console.log('rnlog response ', response);
          if (data.timeLimit > 0) {
            data._id = response.data;
            data.timeLimit = new Date(data.timeLimit);
            this._notify(data);
          }

          this._reload();
          this.props.clearTask();
          this.props.showOrHideModalTaskForm();
          Toast.show('SUCCESSFULLY SAVED TASK', Toast.LONG);

        })
        .catch(error => {
          // console.log('rnlog error ', error);
          // Alert.alert('Error', error.message)
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        })
    }
  };

  _searchSuggestion = (text) => {
    this.setState({search: text});

    if (text.length >= 2) {
      Api.call(ApiRoutes.TASK_RELATED_TYPE_A_HEAD, {search: text})
        .then(response => {
          if (response.data.length > 0) {
            this.setState({suggestions: response.data});
          }
        })
        .catch(error => {
          // Alert.alert('Error', 'the task could not be saved.')
          Toast.show('ERROR the task could not be saved.', Toast.LONG);
        });
    }
  };

  _selectLead = (item) => {
    this.setState({
      relatedId: item._id,
      relatedLabel: item.label,
      relatedCollection: item.relatedCollection,
      suggestions: [],
      search: ''
    });
  };

  _renderItem = ({item}) => (
    <TouchableOpacity onPress={() => this._selectLead(item)} style={styles.suggestionsLead}>
      <View style={styles.column9}>
        <Text style={styles.fwb}>{item.label}</Text>
      </View>

      <View style={styles.column1}>
        <Icon
          name={"account"}
          type={"material-community"}
        />
      </View>

    </TouchableOpacity>
  );

  _notify = (notification) => {
    pushNotifications.cancelLocalNotifications(notification.appNotificationId.toString());

    pushNotifications.localNotificationSchedule({
      id: notification.appNotificationId.toString(),
      title: 'Tarea Pendiente para el ' + notification.timeLimit,
      message: notification.name,
      date: notification.timeLimit,
      vibrate: true,
      group: notification.taskListId,
      task: notification
    });
  };

  render() {
    // console.log('rnlog date backend ', this.state.timeLimit);

    return (
      <ScrollView
        contentContainerStyle={{ flex: 1 }}
      >
        {
          this.state.loading &&
          <Spinner
            animation={'fade'}
            visible={this.state.loading}
            textStyle={{textAlign: 'center', width: '100%', color: Constant.colors.darkBlue}}
            textContent={"Loading..."}
            size={"large"}
            overlayColor={"white"}
            color={Constant.colors.darkBlue}
          />
        }
        <View style={{flex: 1}}>

          <TextInput
            style={{color: Constant.colors.darkBlue}}
            onChangeText={name => this.setState({name})}
            placeholder='Task Name...'
            placeholderTextColor={Constant.colors.darkBlue}
            underlineColorAndroid={Constant.colors.darkBlue}
            value={this.state.name}
            autoCapitalize={'none'}
          />

          {
            this.state.showErrorName &&
            <Text style={{color: Constant.colors.redError}}>This field is required</Text>
          }

          <View style={[styles.alignItemsCenter, {marginTop: 10}]}>
            <DatePicker
              date={this.state.timeLimit}
              onDateChange={(date) => this.setState({timeLimit: date})}
              style={[styles.center, {width: '90%'}]}
              customStyles={
                {
                  dateInput: {borderWidth: 1, borderColor: Constant.colors.darkBlue, borderRadius: 5},
                  placeholderText: { color: Constant.colors.darkBlue},
                  dateText:{ color: Constant.colors.darkBlue,justifyContent: 'flex-start'}
                }
              }
              is24Hour={true}
              mode="datetime"
              placeholder="Select date"
              format="YYYY/MM/DD HH:mm:ss"
              androidMode="spinner"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
            />
          </View>



          <TextInput
            style={{color: Constant.colors.darkBlue}}
            onChangeText={this._searchSuggestion}
            placeholder='Search'
            placeholderTextColor={Constant.colors.darkBlue}
            underlineColorAndroid={Constant.colors.darkBlue}
            value={this.state.search}
            autoCapitalize={'none'}
          />

          <View style={[styles.flexRow, styles.alignItemsCenter, {marginLeft: 3}]}>
            <Text style={[styles.f12, styles.fontRobotoBold, {color: Constant.colors.darkBlue}]}>Lead Selected: </Text>
            <Text style={[styles.f12, styles.fontRobotoBold, {color: Constant.colors.darkBlue}]}>{this.state.relatedLabel}</Text>
          </View>

          <View>
            {
             this.state.suggestions.length > 0 &&
              <FlatList
                data={this.state.suggestions}
                keyExtractor={item => item._id}
                renderItem={this._renderItem}
                inverted
              />
            }

          </View>

          <ImageBackground style={{width: '100%', height: 350, backgroundColor: 'transparent'}} source={this.state.description != '' ? null : require('./../assets/images/description-empty.jpg')}>
            <ScrollView>
              <TextInput
                tyle={{color: Constant.colors.darkBlue}}
                onChangeText={description => this.setState({description})}
                placeholder='Description'
                placeholderTextColor={Constant.colors.darkBlue}
                value={this.state.description}
                autoCapitalize={'none'}
                multiline={true}
                maxHeight={300}
              />
            </ScrollView>

          </ImageBackground>


        </View>

        {
          !this.state.keyboard &&

        <View style={[styles.alignItemsCenter, styles.justifyEnd, {flex: .2}]}>

          <TouchableOpacity
            style={[styles.flexRow, styles.center, styles.bw1, styles.br5, styles.buttonSaveList]}
            onPress={this._saveTask}
          >
            <Text style={[styles.fontRobotoLight, {marginLeft: 5, color: Constant.colors.darkBlue}]}>Save</Text>
          </TouchableOpacity>
        </View>
        }

      </ScrollView>
    )
  }
}

const mapStateToProps = ({taskList, task}) => ({taskList, task});
const mapDispatchToProps = (dispatch) => ({
  clearTask: () => dispatch(clearTask()),
  setTasksList: (payload) => dispatch(setTasksList(payload)),
  setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  showOrHideModalTaskForm: () => dispatch(showOrHideModalTaskForm())
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);
