import React from 'react';
import {View, ImageBackground, TouchableOpacity, Text, BackHandler} from 'react-native';
import {Icon} from 'react-native-elements';
import {connect} from 'react-redux';

import styles from './../assets/styles';
import ArrowLeftIcon from './icons/ArrowLeftIcon';
import {takePicture, clearFile} from "./../actions";
import PreviewFileBottomBtn from './PreviewFileBottomBtn';
import NavigationService from "../NavigationService";


class PreviewFile extends React.Component {

  static navigationOptions = {
    header: null
  };

  _takeAnotherpicture = () => {
    this.props.takePicture();
  };

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', () => this._reset());
  }

  _reset = () => {
    this.setState({loading: false, disableBtn: false});
    this.props.takePicture();
    this.props.clearFile();
    NavigationService.navigate('messages');
  };

  _renderImageDocument = () => {
    return (
      <View style={styles.previewFileContent}>
        <View style={[styles.remitentChatList, styles.alignItemsCenter, styles.justifyStart, {height: 50, width: '100%', flexDirection: 'row'}]}>
          <TouchableOpacity style={{marginLeft: 30}} onPress={() => this._reset()}>
            <ArrowLeftIcon height={25}/>
          </TouchableOpacity>

          <Text style={[styles.f18, {color: Constant.colors.darkBlue, marginLeft: 30, fontFamily: 'Roboto-Bold'}]}>PREVIEW FILE</Text>
        </View>
        <ImageBackground
          source={{uri: this.props.file.uri}}
          style={{flex: 1}}
        >
          <PreviewFileBottomBtn />
        </ImageBackground>
      </View>
    )
  };

  _renderFileDocument = () => {
    return(
      <View style={styles.previewFileContent}>
        <View style={[styles.remitentChatList, styles.alignItemsCenter, styles.justifyStart, {height: 50, width: '100%', flexDirection: 'row'}]}>
          <TouchableOpacity style={{marginLeft: 30}} onPress={() => this._reset()}>
            <ArrowLeftIcon height={25}/>
          </TouchableOpacity>

          <Text style={[styles.f18, {color: Constant.colors.darkBlue, marginLeft: 30, fontFamily: 'Roboto-Bold'}]}>PREVIEW FILE</Text>
        </View>
        <View style={[styles.remitentChatList, styles.alignItemsCenter, styles.justifyStart, {height: 50, width: '100%', flexDirection: 'row'}]}>
          <TouchableOpacity style={{marginLeft: 30}} onPress={() => this._reset()}>
            <ArrowLeftIcon height={25}/>
          </TouchableOpacity>

          <Text style={[styles.f18, {color: Constant.colors.darkBlue, marginLeft: 30, fontFamily: 'Roboto-Bold'}]}>{from}</Text>
        </View>

        <View style={styles.previewFileDocument}>
          <Icon name={'file'} size={80} color={Constant.colors.darkBlue}
            type='material-community'
          />
          <Text style={styles.previewFileDocumentText}>{this.props.file.fileName}</Text>
        </View>

        <View style={styles.previewFileBottomBtn}>
          <PreviewFileBottomBtn />
        </View>

      </View>
    )
  };

  render() {
    if(this.props.file.typeFile === 'file') {
      return this._renderFileDocument();
    }else {
      return this._renderImageDocument();
    }

  }
}

const mapStateToProps = ({file}) => ({file});

const mapDispatchToProps = (dispatch) => ({
  takePicture: () => dispatch(takePicture()),
  clearFile: () => dispatch(clearFile()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PreviewFile);