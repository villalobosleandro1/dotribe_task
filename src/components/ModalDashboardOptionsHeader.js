import React from 'react';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from "react-native";
import Toast from "react-native-simple-toast";

import {setUser} from "./../actions";
import styles from "./../assets/styles";
import CloseIcon from './icons/CloseIcon';


class ModalDashboardOptionsHeader extends React.Component {

  componentDidMount() {
    if(Object.keys(this.props.user).length === 0) {
      Api.call(ApiRoutes.GET_USER, {}, 'get')
        .then(response => {
          this.props.setUser({
            _id: response._id,
            name: `${response.name} ${response.lastName}`,
            identification: response.identification,
            email: response.email,
            urlImg: response.urlImg
          });
        })
        .catch(error => {
          // console.log('rnlog error get User:', error);
          Toast.show('ERROR WHEN LOADING USER INFORMATION', Toast.LONG);
        });
    }
  }

  render() {
    return (
      <View style={[styles.modalDashboardOptionItem, {marginVertical: 5}]}>

        <View style={[styles.column8, {paddingLeft: 9}]}>
          <Text style={[styles.f16, styles.fontRobotoLight, {color: Constant.colors.darkBlue}]} numberOfLines={1}>{this.props.user.name.toUpperCase()}</Text>
          <Text style={[styles.f12, styles.fontRobotoRegular, {color: Constant.colors.darkBlue}]} numberOfLines={1}>{this.props.user.email}</Text>
        </View>

        <TouchableOpacity
          onPress={this.props.onPress}
          style={[styles.alignItemsCenter, {flex: 1}]}
        >
          <CloseIcon/>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  setUser: (payload) => dispatch(setUser(payload))
});


const mapStateToProps = ({user}) => ({user});

export default connect(mapStateToProps, mapDispatchToProps)(ModalDashboardOptionsHeader);
