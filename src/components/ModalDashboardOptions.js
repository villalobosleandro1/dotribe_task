import React from 'react';
import Modal from "react-native-modal";
import {Alert, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {connect} from 'react-redux';
import Toast from "react-native-simple-toast";

import {newTaskList, setTasksList, setTasksListSelected, showOrHideModalDashboard, load, showOrHideModalTaskListForm} from "./../actions";
import ModalDashboardOptionsHeader from "./ModalDashboardOptionsHeader";
import styles from "./../assets/styles";
import AddIcon from './icons/AddIcon';



class ModalDashboardOptions extends React.Component {
  _reload = () => {
    Api.call(ApiRoutes.TASKS_LIST_BY_USER_LOGGED_APP)
      .then(response => {
          console.log('rnlog aqui ', response);
        this.props.setTasksList(response.list);
        this.props.setTasksListSelected(response.listSelected);
        this.props.load();
      })
      .catch(error => {
        if (typeof error.stopAlerts === 'undefined' || !error.stopAlerts) {
          // Alert.alert('Error', error.message);
          Toast.show(`Error ${error.message}`, Toast.LONG);
        }
      });
  };

  _confirmDelete = () => {
    Alert.alert(
      'Are you sure you want to delete task list?',
      'Once this action is executed, it will not be possible to restore the list.',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: () => this._deleteList()},
      ],
      { cancelable: false }
    );
  };

  _deleteList = () => {
    this.props.load();

    Api.call(ApiRoutes.TASK_LIST_REMOVE, {_id: this.props.taskList.selected._id})
      .then(response => {
        this.props.setTasksListSelected({});
        this._reload();
        this.props.showOrHideModalDashboard();
        Toast.show('List deleted successful', Toast.LONG);
      })
      .catch(error => {
        // Alert.alert('Error', 'try later')
        Toast.show(`Error ${error.message}`, Toast.LONG);
      });
  };

  _setSelectedList = (_id) => {
      console.log('rnlog entro ', _id);
    this.props.load();
    Api.call(ApiRoutes.TASKS_LIST_SET_SELECTED(_id), {}, 'PUT')
      .then(response => {
        console.log('rnlog response ', response);
        this._reload();
        this.props.showOrHideModalDashboard();
      })
      .catch(error => {
          console.log('rnlog error set ', error);
          Alert.alert('Error', error.message)
      });
  };

  createNewList = () => {
    this.props.newTaskList(true);
    this.props.showOrHideModalDashboard();
    this.props.showOrHideModalTaskListForm();
  };

  render () {
    const taskListName = this.props.taskList.selected.name,
      options =  [
        {
          name: 'Create Task List',
          icon: 'plus',
          fun: () => {
            this.props.newTaskList(true);
            this.props.showOrHideModalDashboard();
            this.props.showOrHideModalTaskListForm();
          }
        },
        {
          name: `Rename ${taskListName}`,
          icon: 'pencil',
          fun: () => {
            this.props.newTaskList(false);
            this.props.showOrHideModalDashboard();
            this.props.showOrHideModalTaskListForm();
          }
        },
        {
          name: `Delete ${taskListName}`,
          icon: 'delete',
          fun: () => this._confirmDelete()
        }
      ],
      list = this.props.taskList.list;

    return (
      <View>
        <Modal
          isVisible={this.props.showModalDashboard}
          swipeDirection={'down'}
          backdropOpacity={.3}
          style={styles.modalUploadFileOrImage}
          onBackButtonPress={() => this.props.showOrHideModalDashboard()}
        >
          <View style={styles.modalBottomContainerDashboard}>
            <View style={styles.column1}>

              <ModalDashboardOptionsHeader onPress={this.props.showOrHideModalDashboard}/>

              <View style={styles.column9}>
                <ScrollView>
                  {/*estas son todas las listas*/}
                  <View style={[styles.modalDashboardOptionItem, styles.column, styles.alignItemsStart, {borderBottomColor: 'white'}]}>
                    <Text style={[styles.f14, styles.fontRobotoLight, {paddingBottom: 12, paddingHorizontal: 9, color: Constant.colors.darkBlue}]}>All Lists</Text>
                    {
                      list.map((item, i) => (
                        <TouchableOpacity
                          key={i}
                          onPress={() => this._setSelectedList(item._id)}
                          style={[
                            styles.stretch,
                            {
                              paddingHorizontal: 16,
                              paddingVertical: 5,
                            },
                            item._id === this.props.taskList.selected._id ? { borderColor: Constant.colors.darkBlue, borderRadius: 5,borderWidth: 1} : null
                          ]}
                        >
                          <Text style={[styles.fontRobotoBold, {color:  Constant.colors.darkBlue}]} numberOfLines={1}>{item.name}</Text>
                        </TouchableOpacity>
                      ))
                    }
                  </View>

                  {/*{*/}
                    {/*options.map((opt, i) => (*/}
                      {/*<TouchableOpacity*/}
                        {/*key={i}*/}
                        {/*style={styles.modalDashboardOptionItem}*/}
                        {/*onPress={opt.fun}*/}
                      {/*>*/}
                        {/*<Icon*/}
                          {/*name={opt.icon}*/}
                          {/*color={Constant.colors.secondaryGrayColor}*/}
                          {/*type={'material-community'}*/}
                          {/*containerStyle={[styles.column1, styles.center]}*/}
                        {/*/>*/}
                        {/*<Text style={[styles.textMuted, styles.column9]} numberOfLines={1}>{opt.name}</Text>*/}
                      {/*</TouchableOpacity>*/}
                    {/*))*/}
                  {/*}*/}


                </ScrollView>
                <View style={{alignItems: 'center', marginBottom: 15}}>
                  <TouchableOpacity
                    style={[styles.flexRow, styles.center, styles.bw1, styles.br5, styles.buttonCreateList]}
                    onPress={this.createNewList}
                  >
                    <AddIcon
                      width={12}
                      height={20}
                      strokeWidth={40}
                    />
                    <Text style={[styles.fontRobotoLight, {marginLeft: 5, color: Constant.colors.darkBlue}]}>Create New List</Text>
                  </TouchableOpacity>

                </View>
              </View>

            </View>


          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = ({taskList, modal}) => ({taskList, showModalDashboard: modal.showModalDashboard});

const mapDispatchToProps = (dispatch) => ({
  setTasksList: (payload) => dispatch(setTasksList(payload)),
  setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  newTaskList: (bool) => dispatch(newTaskList(bool)),
  showOrHideModalDashboard: () => dispatch(showOrHideModalDashboard()),
  load: () => dispatch(load()),
  showOrHideModalTaskListForm: () => dispatch(showOrHideModalTaskListForm())
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalDashboardOptions);
