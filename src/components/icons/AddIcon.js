import React from 'react'
import Svg, { Path } from 'react-native-svg'

const AddIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path fill="none" stroke={color} strokeWidth={strokeWidth} d="M489.8 253H9.9M249.8 13.1V493" />
    </Svg>
  )
}

export default AddIcon;
