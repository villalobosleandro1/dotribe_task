import React from 'react'
import Svg, { Path } from 'react-native-svg'

const ChatIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M332.4 123.1l-275.9.5a48 48 0 0 0-47.9 48v170.8a48 48 0 0 0 47.9 47.9h49.1v80.8a8.1 8.1 0 0 0 5.2 7.5 8.3 8.3 0 0 0 2.9.5 8 8 0 0 0 6-2.6l78.3-86.2 134.4-.6a48 48 0 0 0 47.9-47.9V171a48 48 0 0 0-47.9-47.9zm-32.9 145.4"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path
        d="M396.5 358.7l26.4 29a8.2 8.2 0 0 0 6 2.6 8.4 8.4 0 0 0 2.9-.6 8 8 0 0 0 5.2-7.5v-80.8h8.7a48 48 0 0 0 47.9-47.9V82.7a48 48 0 0 0-47.9-47.9h0l-275.9-.6a48 48 0 0 0-47.9 47.9v25.3m323.6-72.7M112.1 285.2a28.1 28.1 0 1 1 28.1-28.1 28.1 28.1 0 0 1-28.1 28.1zM196.4 285.2a28.1 28.1 0 1 1 28.1-28.1 28.1 28.1 0 0 1-28.1 28.1z"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path
        d="M280.6 285.2a28.1 28.1 0 1 1 28.1-28.1 28.1 28.1 0 0 1-28.1 28.1z"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default ChatIcon;
