import React from 'react'
import Svg, { Path, Ellipse } from 'react-native-svg'

const GlassIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M212.9 226.7c10.5-11.5 24.4-18.5 39.5-18.5s28.1 6.6 38.5 17.5M219.1 253c8.1-11.4 20-18.7 33.3-18.7s23.4 6.2 31.4 16.2"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Ellipse
        cx={134.5}
        cy={255.3}
        rx={84.6}
        ry={75.8}
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Ellipse
        cx={368.8}
        cy={255.3}
        rx={84.6}
        ry={75.8}
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path fill="none" stroke={color} strokeWidth={strokeWidth} d="M449.7 234.4h44.2M8.9 234.4h44.3" />
    </Svg>
  )
}

export default GlassIcon;
