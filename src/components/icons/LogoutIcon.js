import React from 'react'
import Svg, { Path } from 'react-native-svg'

const LogoutIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
        d="M323.9 395.6L472 247.4 323.9 99.3M472 247.4H103.9M237.5 492.9H28.4V16h209.1"
      />
    </Svg>
  )
}

export default LogoutIcon;
