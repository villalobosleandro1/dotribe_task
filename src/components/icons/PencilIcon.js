import React from 'react'
import Svg, { Path } from 'react-native-svg'

const PencilIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M484.6 149.8L141.4 493H16.8l-2-126.5L358.1 23.2a38.4 38.4 0 0 1 54.3 0l72.2 72.2a38.4 38.4 0 0 1 0 54.4zM304.4 76.9l127 126.1"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default PencilIcon;
