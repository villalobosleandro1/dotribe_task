import React from 'react'
import Svg, { Rect, Path } from 'react-native-svg'

const LockIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Rect
        x={50.5}
        y={186.7}
        width={399.8}
        height={305.96}
        rx={53.9}
        ry={53.9}
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Rect
        x={218.2}
        y={261.5}
        width={63.5}
        height={120.87}
        rx={31.4}
        ry={31.4}
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path
        d="M117.7 186.7v-43.3c0-76.6 63.4-138.9 140-135.6a134.1 134.1 0 0 1 128.3 134v44.9"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default LockIcon;
