import React from 'react'
import Svg, { Circle, Path } from 'react-native-svg'

const KeyIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Circle cx={249.5} cy={117.5} r={109.9} fill="none" stroke={color} strokeWidth={strokeWidth} />
      <Path
        d="M249.5 228.9v242.7a20.9 20.9 0 0 0 20.9 20.9h56.7M327.1 389.1h-77.6"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  );
}

export default KeyIcon;