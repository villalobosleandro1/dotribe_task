import React from 'react'
import Svg, { Path } from 'react-native-svg'

const ArrowLeftIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
        d="M203.3 441.5L8.1 246.3 203.3 51.1M8.1 246.3h485"
      />
    </Svg>
  )
}

export default ArrowLeftIcon;
