import React from 'react'
import Svg, { Path } from 'react-native-svg'

const MenuIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
        d="M10.2 427H486M10.2 244.8H486M10.2 64H486"
      />
    </Svg>
  )
}

export default MenuIcon
