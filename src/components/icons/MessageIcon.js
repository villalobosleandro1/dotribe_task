

import React from 'react'
import Svg, { Path } from 'react-native-svg'

const MessageIcon = props => {

  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M68.8 17.5A63.3 63.3 0 0 0 5.6 80.7v225.5a63.3 63.3 0 0 0 63.3 63.2l177.3.7L349.7 484a10.6 10.6 0 0 0 7.9 3.5 11 11 0 0 0 3.8-.7 10.7 10.7 0 0 0 6.8-9.9V370.2H433a63.3 63.3 0 0 0 63.2-63.3V81.5A63.3 63.3 0 0 0 433 18.2l-364.2-.7zm43.4 192"
        fill="#00215a"
        strokeWidth={strokeWidth}
      />
      <Path
        d="M359.6 231.5a37.1 37.1 0 1 0-37.1-37.1 37.1 37.1 0 0 0 37.1 37.1zM248.4 231.5a37.1 37.1 0 1 0-37.1-37.1 37.1 37.1 0 0 0 37.1 37.1zM137.2 231.5a37.1 37.1 0 1 0-37.1-37.1 37.1 37.1 0 0 0 37.1 37.1z"
        fill="#fff"
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default MessageIcon

