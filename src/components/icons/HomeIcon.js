import React from 'react'
import Svg, { Path } from 'react-native-svg'

const HomeIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
        d="M323.1 493h89.5V240.2h70.8L251.1 8 18.9 240.2h70.7V493H179l-.1-100.9v-59.4h144.2V493z"
      />
    </Svg>
  )
}

export default HomeIcon;
