import React from 'react'
import Svg, { Path } from 'react-native-svg'

const MessageOutlineIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M428.9 21.5l-356.4.7a62 62 0 0 0-61.8 62v220.6a62 62 0 0 0 61.9 61.9H136v104.4a10.5 10.5 0 0 0 6.7 9.7 10.7 10.7 0 0 0 3.7.7 10.4 10.4 0 0 0 7.8-3.4l101.2-111.4L429 366a62 62 0 0 0 61.9-61.9V83.4A62 62 0 0 0 429 21.5zm-42.4 187.9"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path
        d="M144.4 231a36.3 36.3 0 1 1 36.3-36.3 36.3 36.3 0 0 1-36.3 36.3zM253.2 231a36.3 36.3 0 1 1 36.3-36.3 36.3 36.3 0 0 1-36.3 36.3zM362.1 231a36.3 36.3 0 1 1 36.3-36.3 36.3 36.3 0 0 1-36.3 36.3z"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default MessageOutlineIcon;
