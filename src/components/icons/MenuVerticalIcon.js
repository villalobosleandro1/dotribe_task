import React from 'react'
import Svg, { Path } from 'react-native-svg'

const MenuVerticalIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M188 69.7a60.6 60.6 0 1 1 60.6 60.6A60.6 60.6 0 0 1 188 69.7zM188 251.6a60.6 60.6 0 1 1 60.6 60.6 60.6 60.6 0 0 1-60.6-60.6zM188 433.5a60.6 60.6 0 1 1 60.6 60.6 60.6 60.6 0 0 1-60.6-60.6z"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default MenuVerticalIcon;
