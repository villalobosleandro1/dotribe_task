import React from 'react'
import Svg, { Path } from 'react-native-svg'

const ArrowDownIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
        d="M492.6 159L250.1 401.5 7.6 159h485z"
      />
    </Svg>
  )
}

export default ArrowDownIcon;
