import React from 'react'
import Svg, { Circle, Path } from 'react-native-svg'

const CloseIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Circle
        cx={250.5}
        cy={251.5}
        r={242.5}
        transform="rotate(-45 250.492 251.543)"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
        d="M327.5 335.5l-168-168M327.5 167.5l-168 168"
      />
    </Svg>
  )
}

export default CloseIcon;
