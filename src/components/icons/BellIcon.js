import React from 'react'
import Svg, { Path } from 'react-native-svg'

const BellIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M112.4 212.9s1.1 89.1-6.9 117.4c-5.7 20.3-32.3 65.3-45.8 87.7a8.6 8.6 0 0 0 7.4 13h367a8.6 8.6 0 0 0 7.4-13.1c-13.5-22.2-39.8-66.6-45.8-86.5-8.7-28.4-11.2-118.5-11.2-118.5v6.9a136 136 0 0 0-272.1 0M286.3 431a42.6 42.6 0 1 1-75.7 0"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path
        d="M219.4 86.8V38.4a30.2 30.2 0 0 1 60.5 0v48.4"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default BellIcon;
