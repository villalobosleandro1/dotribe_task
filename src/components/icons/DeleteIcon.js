import React from 'react'
import Svg, { Path } from 'react-native-svg'

const DeleteIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path
        d="M420 97.4v344.5a51 51 0 0 1-51 51H140.2a51 51 0 0 1-51-51V97.4M24.5 97.4h455.2M163.8 97.4V38.7a30.8 30.8 0 0 1 30.8-30.8h117.5a30.8 30.8 0 0 1 30.8 30.8v58.7M332.9 191.9V376M168.8 191.9V376M253.4 191.9V376"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}

export default DeleteIcon;
