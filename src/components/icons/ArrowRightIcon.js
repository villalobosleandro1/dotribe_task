import React from 'react'
import Svg, { Path } from 'react-native-svg'

const ArrowRightIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Path fill="none" stroke={color}strokeWidth={strokeWidth} d="M150.5 8L393 250.5 150.5 493V8z" />
    </Svg>
  )
}

export default ArrowRightIcon;
