import React from 'react'
import Svg, { Circle, Path } from 'react-native-svg'

const AddCircleIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Circle cx={250.4} cy={250.9} r={242.5} fill="none" stroke={color} strokeWidth={strokeWidth} />
      <Path
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
        d="M364.2 255.9H126.6M245.4 137.1v237.5"
      />
    </Svg>
  )
}

export default AddCircleIcon;
