import React from 'react'
import Svg, { Rect, Path } from 'react-native-svg'

const EmailIcon = props => {
  const {width = Constant.iconProps.width, height = Constant.iconProps.height, strokeWidth = Constant.iconProps.strokeWidth, color = Constant.colors.darkBlue} = props;
  return(
    <Svg data-name="Capa 1" viewBox="0 0 500 500" width={width} height={height}>
      <Rect
        x={10}
        y={73.4}
        width={480.8}
        height={337.33}
        rx={32.8}
        ry={32.8}
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
      <Path
        d="M17.6 85.2l209.5 182.7a36.4 36.4 0 0 0 46.1 0L483 84.9M90.6 336.1l101.8-95.7M410.5 336.3l-101.7-95.1"
        fill="none"
        stroke={color}
        strokeWidth={strokeWidth}
      />
    </Svg>
  )
}


export default EmailIcon;
