import React from 'react';
import styles from "./../assets/styles";
import {View, TouchableOpacity} from "react-native";
import {connect} from 'react-redux';

import ModalDashboardOptions from './ModalDashboardOptions';
import ModalDashboardOptionsRight from './ModalDashboardOptionsRight';
import ModalTaskListForm from './ModalTaskListForm';
import ModalTaskForm from './ModalTaskForm';
import {showOrHideModalDashboard, showOrHideModalDashboardRight, newTaskList, showOrHideModalTaskForm} from "./../actions";
import MenuVerticalIcon from './icons/MenuVerticalIcon';
import AddCircleIcon from './icons/AddCircleIcon';
import MenuIcon from './icons/MenuIcon';

class DashboardNavBarBottom extends React.Component {
  render() {
    return(
      <View style={styles.simpleContainer}>
        <View style={[styles.row, styles.center, styles.alignItemsCenter, styles.margin_h_24]}>

          <TouchableOpacity
            style={[styles.center, styles.column1]}
            onPress={() => this.props.showOrHideModalDashboard()}
          >
            <MenuVerticalIcon
              strokeWidth={40}
            />
          </TouchableOpacity>

          <View style={[styles.p10, styles.column8]}>
            <TouchableOpacity
              onPress={() => {
                this.props.newTaskList(true);
                this.props.showOrHideModalTaskForm()
              }}
              style={[styles.row, styles.center]}
            >
              <AddCircleIcon
                strokeWidth={25}
                width={40}
              />
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            style={[styles.center, styles.column1]}
            onPress={() => this.props.showOrHideModalDashboardRight()}
          >
            <MenuIcon
              strokeWidth={40}
            />
          </TouchableOpacity>

        </View>

        {
          this.props.showModalDashboard &&
          <ModalDashboardOptions navigation={this.props.navigation} />
        }

        {
          this.props.showModalDashboardRight &&
          <ModalDashboardOptionsRight navigation={this.props.navigation} />
        }

        {
          this.props.showModalTaskListForm &&
          <ModalTaskListForm navigation={this.props.navigation}/>
        }

        {
          this.props.showModalTaskForm &&
          <ModalTaskForm navigation={this.props.navigation}/>
        }
      </View>
    )
  }
}

const mapStateToProps = ({taskList, modal}) => ({
  taskList,
  showModalDashboard: modal.showModalDashboard,
  showModalDashboardRight: modal.showModalDashboardRight,
  showModalTaskListForm: modal.showModalTaskListForm,
  showModalTaskForm: modal.showOrHideModalTaskForm
});

const mapDispatchToProps = (dispatch) => ({
  showOrHideModalDashboard: () => dispatch(showOrHideModalDashboard()),
  showOrHideModalDashboardRight: () => dispatch(showOrHideModalDashboardRight()),
  newTaskList: (bool) => dispatch(newTaskList(bool)),
  showOrHideModalTaskForm : () => dispatch(showOrHideModalTaskForm())
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardNavBarBottom);