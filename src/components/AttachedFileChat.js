import React from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import {Icon} from 'react-native-elements';

import styles from "../assets/styles";
import ImageDetail from "./ImageDetail";

class AttachedFileChat extends React.Component{

  state = {
    modalVisibility: false,
    modalMessageVisibility: false
  };

  showModal = (visibility, type, fileUrl) => {
    this.setState({modalMessageVisibility: visibility, type: type, fileUrl: fileUrl});
  };

  getStyle = () => {
    if(this.props.type !== 'image') {
      return styles.containerDocument;
    }
    return styles.containerLeft;
  };

  render() {
    let icon = 'file-document';
    if(this.props.type !== 'application') {
      icon = 'file-video';
    }
    return(
      <View style={this.props.style || {}}>
        {
          this.state.modalMessageVisibility &&
          <ImageDetail
            visibility={this.state.modalMessageVisibility}
            type={this.state.type}
            image={this.state.fileUrl}
            onClose={() => this.setState({modalMessageVisibility: !this.state.modalMessageVisibility})}
          />
        }
        <TouchableOpacity  onPress={() => this.showModal(!this.state.modalVisibility, this.props.type, this.props.url)}
                           style={[this.getStyle(), {backgroundColor: this.props.backgroundColor}]}>
          <View style={styles.containerImage}>
            {
              this.props.type === 'image' &&
              <Image style={{width: '90%', height: '90%'}} source={{uri: this.props.url}}/>
            }

            {
              this.props.type !== 'image' &&
              <Icon
                name={icon}
                size={60}
                color={Constant.colors.primaryLightColor}
                type='material-community'
              />
            }
          </View>

          <View style={[styles.containerText, styles.alignItemsStart, {marginBottom: 10}]}>
            {
              this.props.body !== '' &&
              <Text style={[styles.f14, {color: Constant.colors.darkBlue, marginLeft: 10}]}>{this.props.body}</Text>
            }
            <Text style={[styles.f10, {color: Constant.colors.darkBlue, marginLeft: 10}]}>{this.props.date}</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default AttachedFileChat;