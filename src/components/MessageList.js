import React from 'react';
import {View, Text, FlatList, TextInput, TouchableOpacity, ImageBackground, Image, BackHandler} from 'react-native';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import Meteor from "react-native-meteor";
import SocketIOClient from 'socket.io-client';

import styles from './../assets/styles';
import MessageItem from './MessageItem';
import ModalUploadFileOrImage from './ModalUploadFileOrImage';
import {showOrHideModal, showMore} from "./../actions";
import NavigationService from "../NavigationService";
import AddIcon from './icons/AddIcon';
import ArrowRightIcon from './icons/ArrowRightIcon';
import ArrowLeftIcon from './icons/ArrowLeftIcon';
import Toast from "react-native-simple-toast";


class MessageList extends React.Component {

  state = {
    message: '',
    loading: true,
    buttonDisabled: true,
    socket: SocketIOClient('http://192.168.88.55:9999'),
    sms: [],
  };


  // componentWillUnmount() {
  //   if(this.props.ready) {
  //     Meteor.subscribe(Subscribe.MESSAGES, {conversationId:  this.props.conversation._id, extraData: this.props.auth.token}).stop();
  //   }
  // }

  // esta validacion la tenia Nelson pero explotaba al momento de salir y entrar de nuevo en la conversacion, revisar
  // componentDidUpdate() {
  //   if(this.props.ready && this.state.loading) {
  //     this.setState({loading: false})
  //   }
  // }

  // se reemplazo por esta validacion
  componentDidMount() {
    // if(this.props.messages.length === 0 || this.props.messages.length > 0) {
    //   this.setState({loading: false});
    // }

    this.state.socket.connect();
    this.state.socket.emit(Subscribe.MESSAGES, {conversationId: this.props.conversation._id, limit: this.props.limit});

    BackHandler.addEventListener('hardwareBackPress', ()=> NavigationService.navigate('conversations'));
  }

  _renderItem = ({item}) => (
    <MessageItem message={item} />
  );

  renderMoreMessages = () => {
    if(this.state.sms.length === this.props.limit){
      return(
        <View style={[styles.center, {height: 30, marginTop: 10}]}>
          <TouchableOpacity onPress={() => this.props.showMore()}
                            style={[styles.center, {width: 100, height: 30, borderRadius: 20, borderWidth: 1, borderColor: Constant.colors.primaryGrayColor}]}>
            <Text style={[styles.f14, {color: Constant.colors.primaryGrayColor}]}>Load more</Text>
          </TouchableOpacity>
        </View>
      );
    }else {
      return null;
    }
};

  _sendMessage = () => {
    this.setState({buttonDisabled: false});
    const conversation = this.props.conversation;
    const message = this.state.message.trim();
    let insert = {};

    if(message === '') {
      return false;
    }

    if(this.props.navigation.state.params) {
      insert = {
        to: this.props.navigation.state.params.conversation.relatedPhone,
        originalMessage: this.state.message
      };
    }else {
      insert = {
        to: conversation.who,
        originalMessage: this.state.message,
        leadId: conversation.leadId,
        contactId: conversation.contactId
      };
    }

    Api.call(ApiRoutes.SEND_SMS, insert)
      .then(response => {
        this.props.selectConversation({_id: response.data.id, who: response.data.who, labelContact: response.data.labelContact});
        this.setState({message: '', buttonDisabled: true});
        if(this.props.navigation.state.params) {
          NavigationService.navigate('messages');
        }
      })
      .catch(error => {
        // Alert.alert('Error', 'message not sent, try later.')
        Toast.show('Message not sent, try later.', Toast.LONG);
        this.setState({message: '', buttonDisabled: true});
      });

  };

  _toggleModal = () => this.props.showOrHideModal();


  render() {
    this.state.socket.on(Subscribe.MESSAGES, data => {
      this.setState({sms: data, loading: false});
    });

    let messages = this.state.sms;

    messages = messages.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
    messages = messages.reverse();
    let from;

    if(this.props.conversation) {
      from = this.props.conversation.who;
    }else{
      from = this.props.navigation.state.params.conversation.relatedPhone;
    }



    return (
      <ImageBackground source={Constant.image.backgroundChat} style={[styles.container, styles.alignItemsCenter, {marginHorizontal: 0}]}>
        {
          this.state.loading &&
          <Spinner
            animation={'fade'}
            visible={this.state.loading}
            textStyle={{textAlign: 'center', width: '100%', color: Constant.colors.darkBlue}}
            textContent={"Loading..."}
            size={"large"}
            overlayColor={"white"}
            color={Constant.colors.darkBlue}
          />
        }

        <View style={[styles.remitentChatList, styles.alignItemsCenter, styles.justifyStart, {height: 50, width: '100%', flexDirection: 'row', backgroundColor: 'white'}]}>
          <TouchableOpacity style={{marginLeft: 30}} onPress={()=> NavigationService.navigate('conversations')}>
            <ArrowLeftIcon height={25}/>
          </TouchableOpacity>

          <Text style={[styles.f18, {color: Constant.colors.darkBlue, marginLeft: 30, fontSize: 18, fontFamily: 'Roboto-Bold'}]}>{from}</Text>
        </View>


        <View style={[styles.column9, styles.stretch, {marginHorizontal: 15}]}>
          {
            messages.length > 0 &&
            <FlatList
              data={messages}
              keyExtractor={item => item._id}
              renderItem={this._renderItem}
              inverted
              style={styles.padding_h_15}
              ListFooterComponent={this.renderMoreMessages}
            />
          }

          {
            messages.length === 0 &&
            <View style={[styles.simpleContainer, styles.center]}>
              <Image source={Constant.image.noMessages} style={{width: 188, height: 180}}/>
              <Text style={[styles.f22, styles.textMuted]}>No Messages</Text>
            </View>
          }
        </View>

        {
          this.props.modal.visible &&
          <ModalUploadFileOrImage navigation={this.props.navigation} />
        }

        <View style={[{flexDirection: 'row', height: 60, marginHorizontal: 15}, styles.stretch]}>
          <TouchableOpacity
            style={[styles.column1, styles.stretch, styles.center]}
            onPress={() => this._toggleModal()}
          >
            <AddIcon strokeWidth={40}/>
          </TouchableOpacity>

          <TextInput
            value={this.state.message}
            onChangeText={message => this.setState({message})}
            placeholder='Type a message...'
            placeholderTextColor={Constant.colors.darkBlue}
            autoCapitalize={'none'}
            multiline={true}
            style={[styles.column8, styles.padding_h_15, styles.stretch, styles.center]}
          />

          {
            this.state.buttonDisabled &&
            <TouchableOpacity
              style={[styles.column1, styles.stretch, styles.center]}
              onPress={this._sendMessage}
            >
              <ArrowRightIcon strokeWidth={30}/>
            </TouchableOpacity>
          }

        </View>
      </ImageBackground>
    )
  }
}

const mapStateToProps = ({modal}) => ({modal});

const mapDispatchToProps = (dispatch) => ({
  showOrHideModal: () => dispatch(showOrHideModal()),
  showMore: () => dispatch(showMore())
});


export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
