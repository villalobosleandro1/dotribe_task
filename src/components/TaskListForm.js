import React from 'react';
import {connect} from 'react-redux'
import {View, TextInput, Alert, Text, TouchableOpacity} from 'react-native';
import Toast from "react-native-simple-toast";

import styles from './../assets/styles';
import AddIcon from './icons/AddIcon';

import {newTaskList, setTasksList, setTasksListSelected, showOrHideModalTaskListForm, load} from '../actions';


class TaskListForm extends React.Component {

  state = {
    name: ''
  };

  componentWillMount(){
    if(!this.props.taskList.new) {
      this.setState({name: this.props.taskList.selected.name})
    }
  }

  componentWillUnmount(){
    this.props.newTaskList(true);
  }

  _reload = () => {
    Api.call(ApiRoutes.TASKS_LIST_BY_USER_LOGGED_APP)
      .then(response => {
        this.props.setTasksList(response.list);
        this.props.setTasksListSelected(response.listSelected);
        // this.props.load();
      })
      .catch(error => {
        if (typeof error.stopAlerts === 'undefined' || !error.stopAlerts) {
          // Alert.alert('Error', error.message);
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        }
      });
  };

  _saveList = () => {

    if(this.state.name === ''){
      return Alert.alert('Error', 'You must fill in all the required fields.');
    }

    this.props.load();

    let uri = ApiRoutes.TASK_LIST_INSERT;
    const data = {name: this.state.name};
    let method = 'POST';

    if(!this.props.taskList.new) {
      uri = ApiRoutes.TASK_LIST_UPDATE(this.props.taskList.selected._id);
      method = 'PUT';
      // data._id = this.props.taskList.selected._id;
    }

    Api.call(uri, data, method)
      .then(response => {
        // console.log('rnlog respuesta ', response);
        this.props.load();
        this.setState({name: ''});
        this._reload();
        this.props.showOrHideModalTaskListForm();
      })
      .catch(error => {
        // console.log('rnlog error ', error);
        this.props.load();
        // Alert.alert('Error', 'the task list could not be' +
        //   ' saved.')
        Toast.show('ERROR the task list could not be saved', Toast.LONG);
      });
  };

  render() {
    return (
      <View style={[styles.simpleContainer]}>

        <View style={[styles.column7, styles.justifyCenter]}>
          <TextInput
            value={this.state.name}
            onChangeText={(text) => this.setState({name: text})}
            placeholder={'Type name....'}
            placeholderTextColor={Constant.colors.darkBlue}
            autoCapitalize={'none'}
            underlineColorAndroid={Constant.colors.darkBlue}
            style={{color: Constant.colors.darkBlue}}
          />
        </View>

        <View style={styles.center}>

          <TouchableOpacity
            style={[styles.flexRow, styles.center, styles.bw1, styles.br5, styles.buttonSaveList]}
            onPress={() => this._saveList()}
          >
            <AddIcon
              width={12}
              height={20}
              strokeWidth={40}
            />
            <Text style={[styles.fontRobotoLight, {marginLeft: 5, color: Constant.colors.darkBlue}]}>Save List</Text>
          </TouchableOpacity>


        </View>

      </View>
    )
  }
}


const mapStateToProps = ({taskList}) => ({taskList});
const mapDispatchToProps = (dispatch) => ({
  setTasksList: (payload) => dispatch(setTasksList(payload)),
  setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  newTaskList: (bool) => dispatch(newTaskList(bool)),
  showOrHideModalTaskListForm: () => dispatch(showOrHideModalTaskListForm()),
  load: () => dispatch(load())
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskListForm);
