/* ModalUploadFileOrImage Component */

import React from 'react';
import {View, Text, TouchableOpacity, Alert} from 'react-native';
import Modal from "react-native-modal";
import {Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';

import styles from './../assets/styles';
import {showOrHideModal,setFile} from "./../actions";
import NavigationService from './../NavigationService';


class ModalUploadFileOrImage extends React.Component {

  state = {
    document: ''
  };

  _closeModal(type) {
    this.props.showOrHideModal();
    if(type) {
      this.props.navigation.navigate('messages');
    }
  }

  _savePicture = (aux) => {
    this.props.showOrHideModal();
    const paramType = aux;
    let type = {
      filetype: [],
    };

    if(paramType) {
      switch (paramType) {
        case 'file':
          type.filetype.push(DocumentPickerUtil.pdf());
          break;
        case 'image':
          type.filetype.push(DocumentPickerUtil.images());
          break;
      }
    }

    DocumentPicker.show(type,(error, res) => {
      if(error) {
        NavigationService.navigate('messages');
      }else{
        res.typeFile = paramType;
        this.props.setFile(res);
        NavigationService.navigate('previewFile');
      }
    });
  };

  _takeAPicture = () => {
    this.props.showOrHideModal();
    NavigationService.navigate('takeAPicture')
  };

  render() {
    return (
      <View>
        <Modal
          isVisible={this.props.modal.visible}
          swipeDirection={'down'}
          backdropOpacity={.3}
          style={styles.modalUploadFileOrImage}
          onBackButtonPress={() => this._closeModal()}
        >
          <View style={styles.containerBtnModalBottom}>

            <View style={styles.center}>
              <TouchableOpacity onPress={this._takeAPicture} style={styles.btnModalUploadFile}>
                <Icon
                  name="camera"
                  size={30}
                  color={Constant.colors.primaryLightColor}
                  type='material-community'
                />
              </TouchableOpacity>
              <Text>Take a picture</Text>
            </View>

            <View style={styles.center}>
              <TouchableOpacity onPress={() => this._savePicture('image')} style={styles.btnModalUploadImage}>
                <Icon
                  name="file-image"
                  size={30}
                  color={Constant.colors.primaryLightColor}
                  type='material-community'
                />
              </TouchableOpacity>
              <Text>Image</Text>
            </View>

          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = ({modal}) => ({modal});

const mapDispatchToProps = (dispatch) => ({
  showOrHideModal: () => dispatch(showOrHideModal()),
  setFile: (payload) => dispatch(setFile(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalUploadFileOrImage);