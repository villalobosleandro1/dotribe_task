import React from 'react';
import {View, Text, Image} from 'react-native';
import {Button} from 'react-native-elements';

import styles from "./../assets/styles";
import NavigationService from "../NavigationService";

class NoConection extends React.Component{
  render(){
    return(
      <View style={styles.containerNoConection}>
        <View style={styles.containerImageNoConection}>
          <Image
            source={Constant.image.noConection}
            style={styles.imageNoConection}
          />
        </View>
        <View style={[styles.alignItemsCenter, {marginTop: 20}]}>
          <Text style={{color: Constant.colors.primaryGrayColor}}>No internet connection.</Text>
          <Text style={[styles.alignTextCenter, {color: Constant.colors.primaryGrayColor}]}>Please try later.</Text>
        </View>

        <View style={{marginTop: 30}}>
          <Button
            buttonStyle={[styles.bgSecondary, styles.br50]}
            title="RETRY CONECTION"
            onPress={()=> NavigationService.navigate('Auth')}
          />
        </View>

      </View>
    )
  }
}


export default NoConection;