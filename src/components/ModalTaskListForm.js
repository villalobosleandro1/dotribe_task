import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';

import {showOrHideModalTaskListForm} from './../actions';
import TaskListForm from './../components/TaskListForm';
import styles from "./../assets/styles";
import CloseIcon from './icons/CloseIcon';


class ModalTaskListForm extends React.Component {
  render() {
    return (
      <View>
        <Modal
          isVisible={this.props.showModalTaskListForm}
          swipeDirection={'down'}
          backdropOpacity={.3}
          onBackButtonPress={() => this.props.showOrHideModalTaskListForm()}
        >

          <View style={[styles.p15, {height: 180,backgroundColor: 'white', borderRadius: 4}]}>
            <View style={[styles.stretch, styles.alignItemsCenter, styles.justifySpaceBetween, {flexDirection: 'row'}]}>
              <Text style={[styles.f18, {color: Constant.colors.darkBlue}]}>Name List</Text>
              <TouchableOpacity
                onPress={() => this.props.showOrHideModalTaskListForm()}
              >
                <CloseIcon/>
              </TouchableOpacity>
            </View>

            <View style={[styles.column9, styles.stretch, styles.center]}>
              <TaskListForm navigation={this.props.navigation} />
            </View>

          </View>

        </Modal>
      </View>
    )
  }
}

const mapStateToProps = ({modal}) => ({showModalTaskListForm: modal.showModalTaskListForm});
const mapDispatchToProps = (dispatch) => ({showOrHideModalTaskListForm: () => dispatch(showOrHideModalTaskListForm())});

export default connect(mapStateToProps, mapDispatchToProps)(ModalTaskListForm);