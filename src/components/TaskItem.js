import React from 'react';
import styles from "./../assets/styles";
import {Alert, Text, TouchableOpacity, View} from "react-native";
import Toast from "react-native-simple-toast";

import {connect} from 'react-redux';
import {setTask, setTasksList, setTasksListSelected, showOrHideModalTaskForm} from './../actions';
import ModalTaskForm from './ModalTaskForm';
import NavigationService from './../NavigationService';
import GlassIcon from './icons/GlassIcon';
import PencilIcon from './icons/PencilIcon';


class TaskItem extends React.Component {

  state = {
    completedTask: false
  };

  _reload = () => {
    Api.call(ApiRoutes.TASKS_LIST_BY_USER_LOGGED_APP)
      .then(response => {
        // const result = response.data;
        this.props.setTasksList(response.list);
        this.props.setTasksListSelected(response.listSelected);
      })
      .catch(error => {
        if (typeof error.stopAlerts === 'undefined' || !error.stopAlerts) {
          // Alert.alert('Error', error.message);
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        }
      });
  };

  _completeTask = (item) => {
    this.setState({completedTask: true});
    if(!item.completed) {
      Api.call(ApiRoutes.TASK_COMPLETED(item._id), {}, 'POST')
        .then(response => {
          if(response) {
            this._reload();
            this.setState({completedTask: false});
            Toast.show('TASK COMPLETED', Toast.LONG);
          }


        })
        .catch(error => {
          // Alert.alert('Error', error.message)
          Toast.show(`ERROR ${error.message}`, Toast.LONG);
        })
    }
  };

  _editTask = (item) => {
    this.props.setTask(item);
    this.props.showOrHideModalTaskForm()
  };

  _removeTask = (item) => {
    Api.call(ApiRoutes.TASK_REMOVE(item._id), {}, 'DELETE')
      .then(response => {
        if(response.success) {
          this._reload();
        }
      })
      .catch(error => {
        // Alert.alert('Error', error.message)
        Toast.show(`ERROR ${error.message}`, Toast.LONG);
      });
  };

  _confirmRemoveTask = (item) => {
    Alert.alert(
      'Are you sure you want to delete this task?',
      'Once this action is executed, it will not be possible to restore the task.',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: () => this._removeTask(item)},
      ],
      { cancelable: false }
    )
  };

  render() {

    const title = this.props.item.name;

    const shadow = {
      backgroundColor: '#FFFFFF',
      borderWidth: 1,
      borderColor: '#d8d8d8',
      borderRadius: 4,
      marginBottom: 5
    };

    return (
      <View style={[styles.column, shadow, styles.height100]}>
        {/*esta es la parte de arriba donde muestra el titulo e icono de tarea completada*/}
        <View style={[styles.row, styles.mv5]}>
          {/*esta es la parte del titulo y lead asignado*/}
          <View style={[styles.column9, styles.p10]}>
            <Text style={[styles.fontRobotoBold, {color: Constant.colors.darkBlue}]} numberOfLines={1}>{title}</Text>
            {
              this.props.item.relatedLabel !== undefined &&
              <Text style={[styles.fontRobotoRegular, {color: Constant.colors.darkBlue}]} numberOfLines={1}>Lead: {this.props.item.relatedLabel}</Text>
            }

          </View>

          {/*este es el icono de que la tarea se completo*/}
          <View style={[styles.column1, { paddingTop: 10, paddingRight: 10}]}>
            <TouchableOpacity
              onPress={() => this._completeTask(this.props.item)}
            >
              {
                !this.state.completedTask &&
                <View style={{
                  width: 30,
                  height: 30,
                  borderRadius: 50,
                  borderWidth: 1,
                  borderColor: Constant.colors.darkBlue}}
                />

              }

              {
                this.state.completedTask &&
                <View style={{
                  width: 30,
                  height: 30,
                  borderRadius: 50,
                  backgroundColor: Constant.colors.darkBlue}}
                />
              }

            </TouchableOpacity>
          </View>

        </View>

        {
          !this.props.item.completed &&
            // esta es la parte de los iconos de editar y ver detalle
          <View style={[styles.row, styles.justifyEnd, styles.margin_h_10, {marginBottom: 5}]}>

            <TouchableOpacity
              onPress={() => NavigationService.navigate('taskDetails', {taskId: this.props.item._id})}
              style={{marginRight: 15}}
            >
              <GlassIcon
                height={55}
                width={40}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this._editTask(this.props.item)}
              style={[styles.p5]}
            >
              <PencilIcon/>
            </TouchableOpacity>

          </View>
        }

        {
          this.props.showModalTaskForm &&
          <ModalTaskForm navigation={this.props.navigation}/>
        }


      </View>
    )
  }
}

const mapStateToProps = ({modal}) => ({
  showModalTaskForm: modal.showOrHideModalTaskForm
});

const mapDispatchToProps = (dispatch) => ({
  setTask: (payload) => dispatch(setTask(payload)),
  setTasksList: (payload) => dispatch(setTasksList(payload)),
  setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  showOrHideModalTaskForm : () => dispatch(showOrHideModalTaskForm())
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskItem);
