import React from 'react';
import {View, Alert, Text, TouchableOpacity} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Meteor from "react-native-meteor";

// import {pushNotifications} from './../services';
import styles from './../assets/styles';
import DashboardNavBarBottom from './DashboardNavBarBottom';
import TasksList from './TasksList';
import PencilIcon from './icons/PencilIcon';
import DeleteIcon from './icons/DeleteIcon';
// import {newTaskList, setTasksList, setTasksListSelected, showOrHideModalDashboard, load, showOrHideModalTaskListForm} from "./../actions";
// import {connect} from "react-redux";
import Toast from "react-native-simple-toast";


class Dashboard extends React.Component {

  // state = {
  //   loadData: false
  // };

  componentDidMount(): void {
    this._reload();
  }

  // componentDidUpdate() {
  //   const SERVER_STATUS = Meteor.status();
  //
  //   if(!this.state.loadData && SERVER_STATUS.connected) {
  //     this.setState({loadData: true});
  //     this._reload();
  //   }
  //
  //   // if (this.props.ready) {
  //   //   let notifications = this.props.messageNotification;
  //   //   notifications = notifications.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
  //   //   notifications = notifications.reverse();
  //   //
  //   //   for (let i = 0; i < notifications.length; i++) {
  //   //     this._notify(notifications[i]);
  //   //   }
  //   // }
  // }

  // componentWillUnmount() {
  //   if (this.props.ready) {
  //     Meteor.subscribe(Subscribe.NOTIFICATIONS, {extraData: this.props.auth.token}).stop();
  //   }
  // }

  // _notify = (notification) => {
  //   pushNotifications.localNotification({
  //     title: 'Tienes un mensaje',
  //     message: notification.body,
  //     group: notification.conversationId,
  //     conversation: notification.conversation
  //   });
  //
  //   Api.call(ApiRoutes.SMS_SET_NOTIFY, {_id: notification._id})
  //     .then(response => console.log('rnlog sms.setAppNotify RESPONSE', response))
  //     .catch(err => console.log('rnlog sms.setAppNotify ERROR', err));
  //
  // };

  _reload = () => {
    Api.call(ApiRoutes.TASKS_LIST_BY_USER_LOGGED_APP)
      .then(response => {
        //console.log('rnlog TASKS_LIST_BY_USER_LOGGED_APP', response);
        this.props.setTasksList(response.list);
        this.props.setTasksListSelected(response.listSelected);
      })
      .catch(error => {
        if (typeof error.stopAlerts === 'undefined' || !error.stopAlerts) {
          // Alert.alert('Error', error.message);
          Toast.show(`Error ${error.message}`, Toast.LONG);
        }
      });
  };

  _editListTask = () => {
    this.props.newTaskList(false);
    this.props.showOrHideModalTaskListForm();
  };

  _confirmDelete = () => {
    Alert.alert(
      'Are you sure you want to delete task list?',
      'Once this action is executed, it will not be possible to restore the list.',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: this._deleteList},
      ],
      { cancelable: false }
    );
  };

  _deleteList = () => {
    this.props.load();

    Api.call(ApiRoutes.TASK_LIST_REMOVE(this.props.taskList.selected._id), {}, 'DELETE')
      .then(response => {
        console.log('rnlog response ', response);
        this.props.setTasksListSelected({});
        this._reload();
        this.props.load();
        Toast.show('List deleted successful ', Toast.LONG);
      })
      .catch(error => {
        console.log('rnlog error ', error);
        this.props.load();
        // Alert.alert('Error', 'try later');
        Toast.show(`Error try later ${error.message}`, Toast.LONG);
      });
  };

  render() {
    return (
      <View style={styles.simpleContainer}>
        {
          this.props.loading &&
          <Spinner
            animation={'fade'}
            visible={this.props.loading}
            textStyle={{textAlign: 'center', width: '100%', color: Constant.colors.darkBlue}}
            textContent={"Loading..."}
            size={"large"}
            overlayColor={"white"}
            color={Constant.colors.darkBlue}
          />
        }

        {
          !this.props.loading &&
          <View style={styles.simpleContainer}>

            <View style={[styles.container, styles.column9]}>

              <View
                style={[
                  styles.simpleContainer,
                  styles.justifySpaceBetween,
                  styles.alignItemsCenter,
                  styles.flexRow
                ]}
              >
                <Text style={[styles.f18, styles.fontRobotoLight, {color: Constant.colors.darkBlue}]}>
                  {this.props.taskList.selected.name || 'LIST NOT FOUND'}
                </Text>

                <View style={[styles.flexRow]}>
                  <TouchableOpacity style={{marginRight: 15}} onPress={this._editListTask}>
                    <PencilIcon />
                  </TouchableOpacity>

                  {
                    !this.props.taskList.selected.default &&
                    <TouchableOpacity onPress={this._confirmDelete}>
                      <DeleteIcon/>
                    </TouchableOpacity>
                  }

                </View>
              </View>

              <View style={[styles.column9, styles.stretch]}>
                <TasksList
                  type={this.props.taskList.typeTaskOfList}
                  items={this.props.taskList.taskOfList}
                />
              </View>

            </View>

            <DashboardNavBarBottom navigation={this.props.navigation}/>

          </View>
        }

      </View>
    )
  }
}

// const mapStateToProps = ({taskList, modal}) => ({
//   taskList,
//   showModalDashboard: modal.showModalDashboard,
//   showModalDashboardRight: modal.showModalDashboardRight,
// });
//
// const mapDispatchToProps = (dispatch) => ({
//   setTasksList: (payload) => dispatch(setTasksList(payload)),
//   setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
//   newTaskList: (bool) => dispatch(newTaskList(bool)),
//   showOrHideModalDashboard: () => dispatch(showOrHideModalDashboard()),
//   load: () => dispatch(load()),
//   showOrHideModalTaskListForm: () => dispatch(showOrHideModalTaskListForm())
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

export default Dashboard;
