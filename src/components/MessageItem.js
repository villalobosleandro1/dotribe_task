import React from 'react';
import {View, Text} from 'react-native';

import styles from './../assets/styles';
import AttachedFileChat from './AttachedFileChat';


class MessageItem extends React.Component {

  state = {
    modalVisibility: false,
    modalMessageVisibility: false
  };

  _getContact = (message) => {
    if (message.direction === 'inbound') {
      if (message.conversation.labelContact) {
        return message.conversation.labelContact;
      } else {
        return message.conversation.who;
      }
    } else {
      return 'Tu';
    }
  };

  showModal = (visibility, type, fileUrl) => {
    this.setState({
      modalMessageVisibility: visibility,
      type: type,
      fileUrl: fileUrl
    });
  };

  render() {
    const message = this.props.message;
    const body = message.body;
    const align = message.direction === 'inbound' ? 'flex-start' : 'flex-end';
    const backgroundColor = message.direction === 'inbound' ? 'rgba(204,204,204,1)' : 'rgba(195,214,255,0.5)';
    const medias = message.media ? message.media : [];

    return (
      <View>
        {
          medias.length === 0 &&
          <View
            style={{alignSelf: align, alignItems: align, marginVertical: 10}}>
            <View style={{backgroundColor, padding: 5, borderRadius: 5}}>
              {/*<Text style={[styles.f16, styles.fwb, {color: Constant.colors.darkBlue}]}>*/}
                {/*{this._getContact(message)}*/}
              {/*</Text>*/}
              <Text style={{color: Constant.colors.darkBlue}}>{body}</Text>
            </View>

            <Text style={[styles.f11, {color: Constant.colors.darkBlue}]}>
              {message.fromNow}
            </Text>
          </View>
        }

        {
          medias.length !== 0 &&
          medias.map((media, index) => {
            let aux = media.contentType.split("/", 1);
            return (
              <AttachedFileChat
                key={index}
                onPress={() => this.showModal(!this.state.modalVisibility, aux[0], media.url)}
                type={aux[0]}
                url={media.url}
                body={body}
                date={message.fromNow}
                style={{
                  alignSelf: align,
                  alignItems: align,
                  marginVertical: 10
                }}
                backgroundColor={backgroundColor}
              />
            )
          })
        }
      </View>
    );
  }
}

export default MessageItem;