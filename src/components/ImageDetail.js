import React from 'react';
import {View, StyleSheet, Image, Modal, TouchableWithoutFeedback, WebView} from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";
import PropTypes from 'prop-types';
import {Card, Icon} from "react-native-elements";
import VideoPlayer from 'react-native-video-controls';


class ImageDetail extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      modalVisible: this.props.visibility
    };
  }

  closeModal() {
    this.setState({modalVisible: false});
    this.props.onClose();
  }

  render(){
    let aux = this.props.type.split("/", 1);

    let type;

    if(aux[0] === 'image') {
      type = 'image';
    }else if(aux[0] === 'video' || aux[0] === 'mp4') {
      type = 'video';
    }else {
      type = 'document';
    }

    return(
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() =>  this.closeModal()}>
        <Card containerStyle={styles.container}>

          <View style={[styles.justifyCenter, {height: '10%', width: '100%',alignItems: 'flex-end',}]}>
            <TouchableWithoutFeedback onPress={() => this.closeModal()} style={{alignItems: 'flex-end'}}>
              <Icon
                name={'close'}
                type={'simple-line-icon'}
                containerStyle={styles.margin_h_15}
                size={30}
                color={Constant.colors.darkBlue}
              />
            </TouchableWithoutFeedback>

          </View>

          <View style={[styles.justifyCenter, styles.alignItemsCenter, {height: '90%'}]}>
            {
              type === 'image' &&
              <Image style={{width: '100%', height: '100%'}}
                     source={{uri: this.props.image}}
              />
            }

            {
              type === 'document' &&
              <View style={{height: '100%', width: '100%'}}>
                <WebView
                  source={{uri: 'https://docs.google.com/gview?embedded=true&url=' + this.props.image}}
                  style={{marginTop: 5}}
                  javaScriptEnabled={true}
                  automaticallyAdjustContentInsets={true}
                />
              </View>
            }

            {
              type === 'video' &&
              <VideoPlayer
                source={{uri: this.props.image}}
                disableFullscreen
                disableBack
              />
            }

          </View>

        </Card>
        {
          this.state.loading &&
          <Spinner visible={this.state.loading} animation={'fade'} textStyle={{textAlign:'center', width: '100%', color: Constant.colors.darkBlue}} textContent={"Loading..."} size={"large"} overlayColor={"white"} color={Constant.colors.darkBlue}/>
        }
      </Modal>

    );
  }
}

ImageDetail.propTypes = {
  onPress: PropTypes.func,
};

export default ImageDetail;
