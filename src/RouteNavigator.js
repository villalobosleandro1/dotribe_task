import React from 'react';
import {createStackNavigator, createAppContainer, createSwitchNavigator, TouchableOpacity} from 'react-navigation';

import NavigationService from './NavigationService';
import NavBar from './components/NavBar';
import appRoutes from './routes';
import AuthLoadingScreen from './containers/AuthLoadingScreen';
import NoConection from "./components/NoConection";

class RouteNavigator extends React.Component {
  
  render() {
    const AppNavigator = createStackNavigator(appRoutes.routes, {
      defaultNavigationOptions: {
        headerTitleStyle: {color: Constant.colors.darkBlue, fontSize: 14, fontFamily: 'Roboto-Bold'},
        headerTintColor: Constant.colors.secondaryLightColor,
        headerLeftContainerStyle: {marginLeft: 15},
        headerLeft: (<NavBar/>)
        // headerRight: (<NavBar/>)
      }
    });

    const AppAuthNavigator = createStackNavigator(appRoutes.authRoutes);

    const AppContainer = createAppContainer(createSwitchNavigator(
      {
        AuthLoading: AuthLoadingScreen,
        App: AppNavigator,
        Auth: AppAuthNavigator,
        NoConection : NoConection,
      },
      {
        initialRouteName: 'AuthLoading'
      }
    ));

    return (
      <AppContainer
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef)
        }}
      />
    );

  }
}

export default RouteNavigator;
