import {SET_FILE,CLEAR_FILE} from "./../actions/actionTypes";

const file = (state = {}, action) => {
  switch(action.type) {
    case SET_FILE:
      return action.payload;
    case CLEAR_FILE:
      return {};
    default:
      return state;
  }
};

export default file;