import {
  SET_TASKS_LIST,
  SET_TASKS_LIST_SELECTED,
  NEW_TASK_LIST,
  SET_TASKS_UNDONE,
  SET_TASKS_DONE
} from './../actions/actionTypes';

const initialState = {
  list: [],
  selected: {},
  new: true,
  typeTaskOfList: 'UNCOMPLETED',
  taskOfList: [],
  showCompleted: false
};

const taskList = (state = initialState, action) => {
  switch(action.type) {
    case SET_TASKS_LIST:
      return {...state, list: action.payload};
    case SET_TASKS_LIST_SELECTED:

      let aux = {...state, selected: action.payload};

      if(!state.showCompleted){
        aux.typeTaskOfList =  'UNCOMPLETED';
        aux.showCompleted = false;
        aux.taskOfList = action.payload.undoneTasks || [];
      }

      // return aux;
      return {
        ...state,
        selected: action.payload,
        typeTaskOfList: 'UNCOMPLETED',
        showCompleted: false,
        taskOfList: action.payload.undoneTasks || []
      };

    case SET_TASKS_UNDONE:
      return {...state,
        typeTaskOfList: 'UNCOMPLETED',
        showCompleted: false,
        taskOfList: state.selected.undoneTasks || []
      };
    case SET_TASKS_DONE:
      return {...state,
        typeTaskOfList: 'COMPLETED',
        showCompleted: true,
        taskOfList: state.selected.completedTasks || []
      };
    case NEW_TASK_LIST:
      return {...state, new: action.bool};
    default:
      return state;
  }
};

export default taskList;