import {combineReducers} from 'redux';
import {LOGOUT} from './../actions/actionTypes';

import authentication from './authentication';
import navigationState from './navigationState';
import taskList from "./taskList";
import conversations from './conversations';
import task from './task';
import modal from './modal';
import takePicture from './takePicture';
import limit from './limit';
import file from './file';
import user from './user';

import loading from './loading';

const allReducers = combineReducers({
  authentication,
  navigationState,
  taskList,
  conversations,
  task,
  modal,
  takePicture,
  limit,
  file,
  user,
  loading
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    state = undefined;
  }

  return allReducers(state, action);
};

export default rootReducer;