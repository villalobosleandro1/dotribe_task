import {
  SHOW_OR_HIDE_MODAL,
  SHOW_OR_HIDE_MODAL_DASHBOARD,
  SHOW_OR_HIDE_MODAL_DASHBOARD_RIGHT,
  SHOW_OR_HIDE_MODAL_TASK_LIST_FORM,
  SHOW_OR_HIDE_MODAL_TASK_FORM,
  RESET_ALL_MODALS
} from './../actions/actionTypes';

const initialState = {
  visible: false,
  showModalDashboard: false,
  showModalDashboardRight: false,
  showModalTaskListForm: false,
  showOrHideModalTaskForm: false
};

const modal = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_OR_HIDE_MODAL:
      return {...state, visible: !state.visible};
    case SHOW_OR_HIDE_MODAL_DASHBOARD:
      return {...state, showModalDashboard: !state.showModalDashboard};
    case SHOW_OR_HIDE_MODAL_DASHBOARD_RIGHT:
      return {...state, showModalDashboardRight: !state.showModalDashboardRight};
    case SHOW_OR_HIDE_MODAL_TASK_LIST_FORM:
      return {...state, showModalTaskListForm: !state.showModalTaskListForm};
    case SHOW_OR_HIDE_MODAL_TASK_FORM:
      return {...state, showOrHideModalTaskForm: !state.showOrHideModalTaskForm};
    case RESET_ALL_MODALS:
      return initialState;
    default:
      return state;
  }
};

export default modal;