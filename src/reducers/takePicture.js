import {TAKE_PICTURE, PICTURE_TAKEN} from './../actions/actionTypes';

const initialState = {
  pictureTaken: false,
  urlImage: null
};

const takePicture = (state = initialState, action) => {

  switch (action.type) {
    case TAKE_PICTURE:
      return {pictureTaken: false, urlImage: null};
    case PICTURE_TAKEN:
      return {pictureTaken: true, urlImage: action.url};
    default:
      return state;
  }

};

export default takePicture;