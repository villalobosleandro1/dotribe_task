import {SET_TASK, CLEAR_TASK} from './../actions/actionTypes';

const task = (state = null, action) => {
  switch (action.type) {
    case SET_TASK:
      return action.payload;
    case CLEAR_TASK:
      return null;
    default:
      return state;
  }
};

export default task;