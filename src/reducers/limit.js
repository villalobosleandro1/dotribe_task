import {SHOW_MORE} from './../actions/actionTypes';

  const limit = (state = 10, action) => {
    switch (action.type) {
      case SHOW_MORE:
        return state + 10;
      default:
        return state;
    }
    
  };
  
  export default limit;

  