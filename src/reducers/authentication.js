import {LOGIN, LOGOUT} from './../actions/actionTypes';

const initialState = {isLogged: false, token: null};

const authentication = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {isLogged: true, token: action.token};
    case LOGOUT:
      return {isLogged: false, token: null};
    default:
      return state;
  }
};


export default authentication;