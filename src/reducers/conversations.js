import {
  SET_CONVERSATIONS,
  SELECT_CONVERSATION
} from './../actions/actionTypes';

const initialState = {
  list: [],
  conversation: null
};

const conversation = (state = initialState, action) => {
  switch(action.type) {
    case SET_CONVERSATIONS:
      return {...state, list: action.payload};
    case SELECT_CONVERSATION:
      return {...state, conversation: action.conversation};
    default:
      return state;
  }
};

export default conversation;