import {CHANGE_NAVIGATION_STATE} from './../actions/actionTypes';
const initialState = {
  prevState: {
    params: undefined,
    routeName: 'dashboard'
  },
  currentState: {
    params: undefined,
    routeName: 'dashboard'
  }
};

const navigationState = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_NAVIGATION_STATE:
      return {
        prevState: action.prevState,
        currentState: action.currentState
      };
    default:
      return state;
  }
};

export default navigationState;