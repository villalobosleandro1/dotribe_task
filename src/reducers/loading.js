import {LOAD} from './../actions/actionTypes';

const loading = (state = false, action) => {
  switch(action.type) {
    case LOAD:
      return !state;
    default:
      return state;
  }
};

export default loading;