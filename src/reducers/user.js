import {SET_USER, CLEAR_USER} from './../actions/actionTypes';

const user = (state = {}, action) => {
  switch(action.type) {
    case SET_USER:
      return action.payload;
    case CLEAR_USER:
      return {};
    default:
      return state;
  }
};

export default user;