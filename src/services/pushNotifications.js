// import PushNotification from 'react-native-push-notification';
// import {PushNotificationIOS} from 'react-native';
// import NavigationService from './../NavigationService';
// import store from './../store';
//
// const configure = () => {
//   PushNotification.configure({
//
//     onRegister: function(token) {
//       //process token
//     },
//
//     onNotification: function(notification) {
//
//       if (notification.task) {
//         const auth = store.getState().authentication;
//
//         if(auth.isLogged) {
//           NavigationService.navigate('App');
//         } else {
//           NavigationService.navigate('Auth');
//         }
//
//       }
//
//       if(notification.conversation) {
//         Api.call(ApiRoutes.SMS_SET_READ, {conversationId: notification.conversation._id})
//           .then(response => {
//             NavigationService.navigate('messages', {
//               conversation: {
//                 relatedId: notification.conversation._id,
//                 relatedLabel: notification.conversation.label || notification.conversation.labelContact,
//                 relatedPhone: notification.conversation.who
//               }
//             });
//           }).catch(err => console.log('rnlog sms.setRead ERROR', err));
//       }
//
//       notification.finish(PushNotificationIOS.FetchResult.NoData);
//     },
//
//     permissions: {
//       alert: true,
//       badge: true,
//       sound: true
//     },
//
//     popInitialNotification: true,
//     requestPermissions: true
//   });
// };
//
// const localNotification = (obj) => {
//   PushNotification.localNotification({
//     autoCancel: true,
//     largeIcon: "ic_launcher",
//     smallIcon: "ic_notification",
//     bigText: obj.message,
//     subText: "Notification",
//     color: "blue",
//     vibrate: true,
//     vibration: 300,
//     title: obj.title,
//     message: obj.message,
//     playSound: true,
//     soundName: 'default',
//     tag: 'do_tribe_notification',
//     group: obj.group,
//     // actions: '["GO"]',
//     conversation: obj.conversation
//   });
// };
//
// const localNotificationSchedule = (object) => {
//   PushNotification.localNotificationSchedule(object);
// };
//
// const cancelLocalNotifications = (id) => {
//   PushNotification.cancelLocalNotifications({id});
// };
//
// const setApplicationIconBadgeNumber = (number) => {
//   PushNotification.setApplicationIconBadgeNumber(number);
// };
//
// export {configure, localNotification, localNotificationSchedule, cancelLocalNotifications, setApplicationIconBadgeNumber};
