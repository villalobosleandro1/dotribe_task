import {
  LOGIN,
  LOGOUT,
  CHANGE_NAVIGATION_STATE,
  SET_TASKS_LIST,
  SET_TASKS_LIST_SELECTED,
  SET_CONVERSATIONS,
  SELECT_CONVERSATION,
  SET_TASK,
  CLEAR_TASK,
  SHOW_OR_HIDE_MODAL,
  TAKE_PICTURE,
  PICTURE_TAKEN,
  SHOW_MORE,
  SET_FILE,
  CLEAR_FILE,
  NEW_TASK_LIST,
  SET_USER,
  CLEAR_USER,
  SHOW_OR_HIDE_MODAL_DASHBOARD,
  SHOW_OR_HIDE_MODAL_DASHBOARD_RIGHT,
  SHOW_OR_HIDE_MODAL_TASK_LIST_FORM,
  SET_TASKS_UNDONE,
  SET_TASKS_DONE,
  LOAD,
  SHOW_OR_HIDE_MODAL_TASK_FORM,
  RESET_ALL_MODALS
} from './actionTypes';

export const login = (token) => ({
  type: LOGIN,
  token
});

export const logout = () => ({
  type: LOGOUT
});

export const changeNavigationState = (prevState, currentState) => ({
  type: CHANGE_NAVIGATION_STATE,
  prevState,
  currentState
});

export const setTasksList = (payload) => ({
  type: SET_TASKS_LIST,
  payload
});

export const setTasksListSelected = (payload) => ({
  type: SET_TASKS_LIST_SELECTED,
  payload
});

export const setTaskUndone = () => ({
  type: SET_TASKS_UNDONE
});

export const setTaskDone = () => ({
  type: SET_TASKS_DONE
});

export const setConversations = (payload) => ({
  type: SET_CONVERSATIONS,
  payload
});

export const selectConversation = (conversation) => ({
  type: SELECT_CONVERSATION,
  conversation
});

export const setTask = (payload) => ({
  type: SET_TASK,
  payload
});

export const clearTask = () => ({
  type: CLEAR_TASK
});

export const showOrHideModal = () => ({
  type: SHOW_OR_HIDE_MODAL
});

export const showOrHideModalDashboard = () => ({
  type: SHOW_OR_HIDE_MODAL_DASHBOARD
});

export const showOrHideModalDashboardRight = () => ({
  type: SHOW_OR_HIDE_MODAL_DASHBOARD_RIGHT
});

export const showOrHideModalTaskListForm = () => ({
  type: SHOW_OR_HIDE_MODAL_TASK_LIST_FORM
});

export const takePicture = () => ({
  type: TAKE_PICTURE
});

export const pictureTaken = (url) => ({
  type: PICTURE_TAKEN,
  url
});

export const showMore = () => ({
  type: SHOW_MORE
});

export const setFile = (payload) => ({
  type: SET_FILE,
  payload
});

export const clearFile = () => ({
  type: CLEAR_FILE
});

export const newTaskList = (bool) => ({
  type: NEW_TASK_LIST,
  bool
});

export const setUser = (payload) => ({
  type: SET_USER,
  payload
});

export const clearUser = () => ({
  type: CLEAR_USER
});

export const load = () => ({
  type: LOAD
});

export const showOrHideModalTaskForm = () => ({
  type: SHOW_OR_HIDE_MODAL_TASK_FORM
});

export const resetAllModals = () => ({
  type: RESET_ALL_MODALS
});