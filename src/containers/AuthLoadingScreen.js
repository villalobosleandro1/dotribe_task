import React from 'react';
import {ActivityIndicator, StatusBar, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {login, setUser, logout, clearUser} from "./../actions";
import Video from "react-native-video";
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

import styles from './../assets/styles';

class AuthLoadingScreen extends React.Component {

  state = {};

  componentDidMount() {
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (token && !this.props.auth.isLogged) {
        this.props.login(token);
      }

      this.setState({showSplash: token ? false : true});

      if (token) {
        Api.get(ApiRoutes.GET_USER)
          .then(response => {
            console.log('rnlog response111111 ', response);
            this.props.setUser({
              _id: response._id,
              name: response.name,
              email: response.email,
              urlImg: response.userImg,
              idPhonePbx: response.idPhonePbx
            });
            this.props.navigation.navigate('App');
          })
          .catch(error => {
            // console.log('rnlog error ', error);
            Toast.show('ERROR WHEN LOADING USER INFORMATION', Toast.LONG);
            this.props.logout();
            this.props.clearUser();
            this.props.navigation.navigate('Auth');
          });
      } else {
        setTimeout(() => this.props.navigation.navigate('Auth'), 4000);
      }

    } catch (e) {
      console.log('rnlog ERROR APP', e)
    }
  };


  render() {
    return (
      <View style={styles.container}>
        {
          typeof this.state.showSplash !== 'undefined' &&
          !this.state.showSplash &&
          <View style={styles.container}>
            <ActivityIndicator
              color={Constant.colors.darkBlue}
              size={'large'}
            />
            <StatusBar barStyle="default"/>
            <Text style={[styles.textContent, {color: Constant.colors.darkBlue}]}>
              Loading...
            </Text>
          </View>
        }

        {
          typeof this.state.showSplash !== 'undefined' &&
          this.state.showSplash &&
          <View style={styles.container}>
            <Video
              source={Constant.splash}
              style={{
                position: 'absolute',
                height: 400,
                width: 400
              }}
              muted={true}
              repeat={false}
              resizeMode="cover"
            />
          </View>
        }
      </View>
    );
  }
}

const mapStateToProps = ({authentication}) => ({auth: authentication});
const mapDispatchToProps = (dispatch) => ({
  login: (token) => dispatch(login(token)),
  setUser: (payload) => dispatch(setUser(payload)),
  logout: () => dispatch(logout()),
  clearUser: () => dispatch(clearUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);
