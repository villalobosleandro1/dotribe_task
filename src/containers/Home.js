import React from 'react';
import {connect} from 'react-redux';
// import Meteor, {withTracker} from 'react-native-meteor';

import Dashboard from './../components/Dashboard';
import {
  setTasksList,
  setTasksListSelected,
  load,
  newTaskList,
  showOrHideModalDashboard,
  showOrHideModalTaskListForm
} from './../actions';

const mapStateToProps = ({taskList, loading, authentication, modal}) => ({
  taskList,
  loading,
  auth: authentication,
  // taskList,
  showModalDashboard: modal.showModalDashboard,
  showModalDashboardRight: modal.showModalDashboardRight,
});

const mapDispatchToProps = (dispatch) => ({
  // setTasksList: (payload) => dispatch(setTasksList(payload)),
  // setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  // load: () => dispatch(load()),
  setTasksList: (payload) => dispatch(setTasksList(payload)),
  setTasksListSelected: (payload) => dispatch(setTasksListSelected(payload)),
  newTaskList: (bool) => dispatch(newTaskList(bool)),
  showOrHideModalDashboard: () => dispatch(showOrHideModalDashboard()),
  load: () => dispatch(load()),
  showOrHideModalTaskListForm: () => dispatch(showOrHideModalTaskListForm())
});

// const wt = withTracker(params => {
//   const handle = Meteor.subscribe(Subscribe.NOTIFICATIONS, {
//     extraData: params.auth.token,
//     subQuery: {
//       $or: [
//         {appNotify: true},
//         {appNotify: {$exists: false}}
//       ]
//     }
//   });
//
//   let messageNotification = Meteor.collection(Subscribe.NOTIFICATIONS).find({});
//
//   return {
//     ready: handle.ready(),
//     messageNotification
//   }
//
// })(Dashboard);

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
