import React from 'react';
import {View, Text, FlatList, TextInput, TouchableOpacity, Alert, KeyboardAvoidingView} from 'react-native';
import {Icon, Avatar, Divider} from 'react-native-elements'

import styles from "../assets/styles";
import NavigationService from "../NavigationService";
import Toast from "react-native-simple-toast";

class SearchContact extends React.Component {

  state = {
    loading: true,
    searchLead: false,
    suggestions: [],
    colorCombination:[
      {
        color: Constant.colors.primaryLightColor,
        backgroundColor: Constant.colors.primaryDarkColor,
      },
      {
        color: Constant.colors.primaryLightColor,
        backgroundColor: Constant.colors.secondaryLightColor,
      },
    ],
    colorCombinationIdx: 0
  };

  clearInput = () => {
    this.setState({searchLead: !this.state.searchLead, suggestions: []});
    this.textInput.clear()
  };

  handleQueryChange = (query) => {

    this.setState(state => ({ ...state, query: query || "" }));
    if(query.length === 0) {
      this.setState({suggestions: []});
    }
    else if(query.length > 2) {
      Api.call(ApiRoutes.TYPE_A_HEAD, {search: query})
        .then(response => {
          this.setState({suggestions: response})
        })
        .catch(error => {
          // Alert.alert('Error', error.message)
          Toast.show(`ERROR ${error.message}`, Toast.LONG)
        });

    }
  };

  _selectConversation = (item) => {
    const conversation = {
      relatedId: item.item._id,
      relatedLabel: item.item.name,
      relatedPhone: item.item.phone,
      newConversation: true
    };

    this.setState({suggestions: [], searchLead: false});
    NavigationService.navigate('messages', {conversation});
  };


  render() {
    return(
      <KeyboardAvoidingView style={[styles.alignItemsCenter, styles.column1, styles.justifySpaceBetween]}>
        <View style={[ styles.flexRow, styles.stretch, styles.alignItemsCenter, styles.height50, styles.justifySpaceBetween, styles.container]}>
          <View style={{width: '90%'}}>
            <TextInput
              style={[styles.margin_h_15, styles.justifyCenter, styles.alignItemsCenter, styles.alignTextCenter]}
              onChangeText={this.handleQueryChange}
              placeholder='Search by Lead or phone number'
              placeholderTextColor={Constant.colors.darkBlue}
              underlineColorAndroid={Constant.colors.darkBlue}
              autoCapitalize={'none'}
              autoFocus
              ref={input => { this.textInput = input }}
            />
          </View>

          <View style={[styles.alignItemsStart, {width: '10%'}]}>
            <Icon
              name={'close'}
              type='material-community'
              color={Constant.colors.darkBlue}
              onPress={()=> this.clearInput()}
            />
          </View>

        </View>

        {
          this.state.suggestions.length === 0 &&
            <View style={[styles.justifyCenter, styles.alignItemsCenter, styles.column9, styles.stretch]}>
              <Icon
                name={'account-search'}
                type='material-community'
                color={Constant.colors.darkBlue}
                size={200}
              />
            </View>

        }

        {
          this.state.suggestions.length > 0 &&
          <View style={[styles.column9, styles.alignItemsStart, styles.justifyStart, styles.margin_h_15, styles.stretch]}>
            <FlatList
              data={this.state.suggestions}
              keyExtractor={item => item._id}
              renderItem={(item) => {
                let backgroundColor = this.state.colorCombination[1].backgroundColor;
                let iconColor = Constant.colors.primaryLightColor;
                if(item.index % 2 === 0) {
                  backgroundColor = this.state.colorCombination[0].backgroundColor;
                  iconColor = this.state.colorCombination[0].color;;
                }
                return(
                  <View>
                    <TouchableOpacity style={[styles.sugestionContact]} onPress={() => this._selectConversation(item)}>
                      <View style={styles.viewAvatarContact}>
                        <Avatar
                          medium
                          rounded
                          icon={{name: 'account', type: 'material-community', color: iconColor}}
                          activeOpacity={1}
                          overlayContainerStyle={{backgroundColor: backgroundColor}}
                        />
                      </View>

                      <View style={styles.textLabelContact} >
                        <Text style={[styles.f18, styles.fwb]}>{item.item.name}</Text>
                        <Text>{item.item.phone}</Text>
                      </View>

                    </TouchableOpacity>

                    <Divider style={[styles.margin_h_15, { backgroundColor: 'black' }]} />
                  </View>

                )
              }}
            />
          </View>
        }
      </KeyboardAvoidingView>
    )
  }
}

export default SearchContact;
