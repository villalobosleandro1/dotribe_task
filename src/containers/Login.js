import React from 'react';
import {View, TextInput, Text, KeyboardAvoidingView} from 'react-native';
import {connect} from 'react-redux';
import {sha512} from 'react-native-sha512';
import {Button} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';

import styles from './../assets/styles';
import {login} from './../actions';
import KeyIcon from './../components/icons/KeyIcon';
import EmailIcon from './../components/icons/EmailIcon';

class Login extends React.Component {

  state = {
    username: 'jose.nieto@finocompany.com',
    password: 'asd123',
    disableBtnLogin: false,
    loading: false,
    hours: new Date().getHours()
  };

  _login = async () => {
    this.setState({loading: true, disableBtnLogin: true});
    const pass = await sha512(this.state.password);

    Api.login({
        username: this.state.username,
        password: pass
      }).then(token => {
        this.props.login(token);
        this.setState({loading: false, disableBtnLogin: false});
        this.props.navigation.navigate('AuthLoading');
      }).catch(error => {
        this.setState({loading: false, disableBtnLogin: false});
        Toast.show('Error Authentication username or password invalid.', Toast.LONG);
      });
  };

  render() {
    let title = 'Good Night.';

    if((this.state.hours >= 5) && (this.state.hours < 12)) {
        title = 'Good Morning.';
    }else if((this.state.hours >= 12) && (this.state.hours < 19)) {
        title = 'Good Afternoon.';
    }

    return (
     <KeyboardAvoidingView  style={styles.container}>
       {
         this.state.loading &&
         <Spinner
           animation={'fade'}
           visible={this.state.loading}
           textStyle={{textAlign: 'center', width: '100%', color: Constant.colors.darkBlue}}
           textContent={"Loading..."}
           size={"large"}
           overlayColor={"white"}
           color={Constant.colors.darkBlue}
         />
       }
       <View style={[styles.column4, styles.stretch, styles.center]}>
         <View style={{
           width: 100,
           height: 100,
           borderRadius: 50,
           backgroundColor: Constant.colors.darkBlue}}
         />
         <Text style={[styles.marginT20, styles.f26, styles.fontRobotoBold, {color: Constant.colors.darkBlue}]}>Hello!</Text>
         <Text style={[styles.marginT10, styles.f16, styles.fontRobotoRegular, {color: Constant.colors.darkBlue}]}>{title}</Text>
       </View>
       <View style={[styles.column6, styles.stretch, styles.alignItemsCenter]}>
         <View style={[styles.flexRow, styles.p5, styles.justifyCenter, styles.alignItemsEnd]}>
           <EmailIcon/>
           <TextInput
             style={[styles.center, styles.alignTextCenter, styles.width200, {color: Constant.colors.darkBlue, marginLeft: 5, borderBottomWidth: 1, borderBottomColor: Constant.colors.darkBlue}]}
             onChangeText={username => {
               this.setState({username})
             }}
             placeholder='Email or Username'
             placeholderTextColor={Constant.colors.darkBlue}
             autoCapitalize={'none'}
           />
         </View>

         <View style={[styles.flexRow, styles.p5, styles.justifyCenter, styles.alignItemsEnd]}>
           <KeyIcon/>
           <TextInput
             style={[styles.center, styles.alignTextCenter, styles.width200, {color: Constant.colors.darkBlue, marginLeft: 5, borderBottomWidth: 1, borderBottomColor: Constant.colors.darkBlue}]}
             onChangeText={password => {
               this.setState({password})
             }}
             placeholder='Password'
             placeholderTextColor={Constant.colors.darkBlue}
             autoCapitalize={'none'}
             secureTextEntry={true}
           />
         </View>

         <View style={[styles.marginT15, styles.center]}>
           <Button
             title="Sign in"
             buttonStyle={[styles.bw1, styles.width150, {backgroundColor: 'transparent', borderColor: Constant.colors.darkBlue}]}
             titleStyle={{color: Constant.colors.darkBlue}}
             onPress={this._login}
             disabled={this.state.disableBtnLogin}
             disabledStyle={{backgroundColor: Constant.colors.primaryGrayColor}}
           />

           {/* <Text style={[styles.marginT10, styles.f16, styles.fontRobotoRegular, {color: Constant.colors.darkBlue}]}>Forgot Password?</Text> */}
         </View>

       </View>
     </KeyboardAvoidingView >
    );
  }
}

const mapDispatchToProps = (dispatch) => ({login: (token) => dispatch(login(token))});

export default connect(null, mapDispatchToProps)(Login);
