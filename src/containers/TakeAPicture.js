/* TakeAPicture Component */

import React from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {Icon} from 'react-native-elements';
import {connect} from 'react-redux';

import styles from "../assets/styles";

import {pictureTaken, setFile} from "./../actions";
import NavigationService from "./../NavigationService";

class TakeAPicture extends React.Component {

  savePicture = async function () {
    if (this.camera) {
      const options = {quality: 0.5, base64: true, exif: true};
      const picture = await this.camera.takePictureAsync(options);

      let aux = picture.uri.split('/');
      let fileName = aux[aux.length - 1];
      let type = fileName.split('.')[1];

      if(type === 'jpg') {
        type = 'jpeg';
      }
      type = 'image/' + type;

      this.props.pictureTaken(picture.uri);
      this.props.setFile({
        uri: picture.uri,
        typeFile: 'image',
        type,
        fileName
      });
      NavigationService.navigate('previewFile');

    }
  };

  render() {
    return (
      <View style={styles.container}>
        {
          !this.props.takePicture.pictureTaken &&
          <RNCamera
            ref={ref => this.camera = ref}
            style={styles.viewCamera}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.auto}
            permissionDialogTitle={'Permission to use camera'}
            permissionDialogMessage={'We need your permission to use your camera phone'}
          >
            <TouchableOpacity
              onPress={this.savePicture.bind(this)}
              style={styles.btnCaptureImage}
            >
              <Icon
                name="camera"
                size={30}
                color={Constant.colors.primaryLightColor}
                type='material-community'
              />
            </TouchableOpacity>
          </RNCamera>
        }
      </View>
    )
  }
}

const mapStateToProps = ({takePicture}) => ({takePicture});

const mapDispatchToProps = (dispatch) => ({
  pictureTaken: (url) => dispatch(pictureTaken(url)),
  setFile: (payload) => dispatch(setFile(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(TakeAPicture);