import React from 'react';
import {connect} from 'react-redux';
// import Meteor, { withTracker } from 'react-native-meteor';
import ConversationList from './../components/ConversationList';
import {load} from "../actions";

// const wt = withTracker(params => {
//   const handle = Meteor.subscribe(Subscribe.CONVERSATIONS, {extraData: params.auth.token});
//   let conversations = Meteor.collection(Subscribe.CONVERSATIONS).find({});
//
//   return {
//     ready: handle.ready(),
//     conversations
//   }
//
// })(ConversationList);

const mapStateToProps = ({authentication, user}) => ({auth: authentication, user});

const mapDispatchToProps = (dispatch) => ({
  load: () => dispatch(load())
});

export default connect(mapStateToProps, mapDispatchToProps)(ConversationList);
