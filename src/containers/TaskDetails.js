import React from 'react';
import {View, Text} from 'react-native';

import {connect} from 'react-redux';
import styles from './../assets/styles';

import {resetAllModals} from './../actions';
import Spinner from "react-native-loading-spinner-overlay";

class TaskDetails extends React.Component {
  state = {
    task: {},
    loading: true
  };

  componentWillMount() {
    this.props.resetAllModals();

    let taskId = this.props.navigation.state.params.taskId;

    Api.call(ApiRoutes.TASK_GET(taskId))
      .then(response => {
          this.setState({task: response, loading: false})
      })
      .catch(err => console.log('rnlog task.get ERROR', err));
  }

  render() {
    const timeLimit =  moment(this.state.task.timeLimit).format('YYYY/MM/DD' +
      ' HH:mm a'),
      completed = this.state.task.completed ? 'TRUE' : 'FALSE';
    return (
      <View style={[styles.container]}>
          {
              this.state.loading &&
              <Spinner
                  animation={'fade'}
                  visible={this.state.loading}
                  textStyle={{textAlign: 'center', width: '100%', color: Constant.colors.darkBlue}}
                  textContent={"Loading..."}
                  size={"large"}
                  overlayColor={"white"}
                  color={Constant.colors.darkBlue}
              />
          }

          {
              !this.state.loading &&
                  <View>
                      <Text style={{maxWidth: '90%'}}>Name: {this.state.task.name}</Text>
                      <Text>Completed: {completed}</Text>
                      <Text>Time Limit: {timeLimit}</Text>
                      <Text>Lead: {this.state.task.relatedLabel}</Text>
                      <Text style={{maxWidth: '90%'}}>Description: {this.state.task.description}</Text>
                  </View>
          }

      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({resetAllModals: () => dispatch(resetAllModals())});

export default connect(null, mapDispatchToProps)(TaskDetails);
