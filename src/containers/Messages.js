import React from 'react';
import {connect} from 'react-redux';
import Meteor, { withTracker } from 'react-native-meteor';
import MessageList from './../components/MessageList';

import {selectConversation, load} from './../actions';


const mapStateToProps = ({authentication, conversations, limit}) => ({auth: authentication, conversation: conversations.conversation,  limit});

const mapDispatchToProps = (dispatch) => ({
  selectConversation: (conversation) => dispatch(selectConversation(conversation)),
  load: () => dispatch(load())
});

// const wt = withTracker(params => {
//   let conversationId;
//
//   if(params.conversation === null) {
//     conversationId = params.navigation.state.params.conversation.relatedId;
//   }else {
//     conversationId = params.conversation._id;
//   }
//
//
//   const handle = Meteor.subscribe(Subscribe.MESSAGES, {conversationId, extraData: params.auth.token, limit: params.limit});
//   let messages = Meteor.collection(Subscribe.MESSAGES).find({});
//
//   return {
//     ready: handle.ready(),
//     messages,
//     conversationId
//   };
//
// })(MessageList);

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
